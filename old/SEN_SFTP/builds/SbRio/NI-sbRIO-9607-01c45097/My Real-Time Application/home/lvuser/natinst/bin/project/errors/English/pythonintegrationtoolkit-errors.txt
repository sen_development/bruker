<?xml version="1.0" encoding="ISO-8859-1"?>
<nidocument>
<nicomment>

<nifamily familyname="Python Integration Toolkit Error Range" displayname="Python Integration Toolkit Error Range">
</nifamily>
</nicomment>
<nierror code="403420">
License Error
</nierror>
<nierror code="403421">
Invalid Python installation
</nierror>
<nierror code="403422">
Invalid Python module
</nierror>
<nierror code="403423">
Can't connect to Python
</nierror>
<nierror code="403424">
Unsupported data type
</nierror>
<nierror code="403425">
Packing error
</nierror>
<nierror code="403426">
Unpacking error
</nierror>
<nierror code="403427">
Python exception
</nierror>
</nidocument>