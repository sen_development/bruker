﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">PowerSuppplyClassLibrary.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../PowerSuppplyClassLibrary.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#285F.31QU+!!.-6E.$4%*76Q!!)$A!!!1@!!!!)!!!)"A!!!!_!!!!!BZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!!!!!+!7!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!X6XSP33#P5/I25'L#E]^GA!!!!Q!!!!1!!!!!)WJK6`7`;F*MZ&lt;I\X^W=H05(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!$\VVALRZ.M2\[Q_+L:L\@!!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%$W?8L!RFK]FV.!58]?.FG]!!!!%!!!!!!!!!,A!!5R71U-!!!!#!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!*735.$!!!!!!)45'^X:8*4&gt;8"Q&lt;(EO&lt;(:D&lt;'&amp;T=R6$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'R16%AQ!!!!-1!"!!9!!!!,5'^X:8*4&gt;8"Q&lt;(E(6(FQ:52F:B6$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'Q!!!!$!!(`!!!!!1!"!!!!!!!$!!!!!!!!!!!!!!!!!!!!!!!$!!!!!A!#!!!!!!!J!!!!,HC=9_"D9'ZAO-!!R)R!T.4!^!0)`M!AQ!%3B:%1S-!!!"'0#B]!!!!!!!"(!!!"'(C=9W$!"0_"!%AR-D#Q&amp;!"J&amp;D2R-!VD5R0A-B?886"R:KA&lt;73'CD%!RZB&gt;!"B.)$KL'!C,(9I(E(4BAQW)W!!9@*N!!!!!!$!!"6EF%5Q!!!!!!!Q!!!F1!!!4A?*SF6-^L%U%5HNUO==H";5&amp;[57A/(J;C)2(V%I8&gt;GM*M7$&amp;?0)I1T5V%3'GN)&amp;N73\@,FBSK.`U0P":&lt;2(;41KK#&lt;,U)AJ*LC5A*(DQ)]=W0**0II?A=XHTPG`?_?=/]G@)%1IN,&amp;]_X9:Z1%*J',KL=OXUHD="(Q`&amp;;1`]V8+%X/]&lt;HIW962B&lt;00'9_]&lt;Y:NP&gt;48:ACL`IRP92YOU;4"#O5;JGX7'#AZ$`/Z[.?07&amp;J%)"H4*&lt;P0CCAWL&amp;]R-D7SA_&amp;S`QB3/.P1%+'_"]=@T7#)TL_RBZ-&gt;G8+LOBS1G]0&lt;U9V(7^&amp;"_]6ODM0$UV&gt;:*,A'=UE95EH8+3,*UU&gt;4%GPYOT&lt;BSGQU8+GCIWECRP\K)I@*6Z&lt;!4&lt;'4R:"%[`@"XMV*##ZQ34$[[$&amp;:,O7%,/0)$&lt;,R%YT-7N1HV19C&amp;F(L?Q,IG+@U%BF$F4G`%.F&gt;3;W$N&lt;G:[.C&gt;DA(=*7+79'N7Y73DD@DB1N//+@VLS5EGLA;&gt;UF$_'HE,N/JM&lt;;@CX/.WH']&amp;20P5/G=!^!Z;Y?G2HCG(6L;1.T5L)+FQ56OUZ;!OS3Y#(G\G:N.O@_)HZ#YL&gt;G$[S^64C"(C9B@4/&gt;;R82ZGA9K\`\35)._'K\N+$QA3*(N0E&gt;]5B\R$.F&lt;9RUN%Y@D2(OEH`E2RJ]='S]\8X@IUV5FTE4]07;2AGBRV.Y6;Z=!KY)\*4C+XQC"!SHHB6AX*/[T/NSDTTV8;1T@-S7Y\SLX+@Y&amp;O#RQ'D[*OMLR3=#4AD]$/"(]:9'P8#P/*`)H*9&lt;]6`U'%5M7C1!!!!Q7!)!:!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"E!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!'1!!"$%W,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!O&lt;E!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!O&gt;(&amp;S^'Z!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!O&gt;(&amp;P\_`P]P2O1!!!!!!!!!!!!!!!!!!!!$``Q!!O&gt;(&amp;P\_`P\_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!0``!-P&amp;P\_`P\_`P\_`P\_`S^%!!!!!!!!!!!!!!!!!``]!R=7`P\_`P\_`P\_`P\``SQ!!!!!!!!!!!!!!!!$``Q$&amp;S]P&amp;P\_`P\_`P\`````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,R&lt;_`P\```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]82````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!S]P,S]P,S]P````````,SQ!!!!!!!!!!!!!!!!$``Q!!R=8,S]P,S``````,U=5!!!!!!!!!!!!!!!!!!0``!!!!!-8,S]P,```,S]5!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!$&amp;S]P,S\]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!R&lt;]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!.-!!5:13&amp;!!!!!"!!*52%.$!!!!!B.1&lt;X&gt;F=F.V=("M?3ZM&gt;G.M98.T&amp;5.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;&amp;2Z='6T,G.U&lt;&amp;"53$!!!!!R!!%!"A!!!!N1&lt;X&gt;F=F.V=("M?1&gt;5?8"F2'6G&amp;5.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;&amp;2Z='6T,G.U&lt;!!!!!-!!0]!!!!"!!%!!!!!!!-!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!+&amp;16%AQ!!!!*Q!"!!5!!!!,5'^X:8*4&gt;8"Q&lt;(E45'^X:8*4&gt;8"Q&lt;(EO&lt;(:D&lt;'&amp;T=Q!$!!!!#?5!!#IT?*T.GH^Q6&amp;=6R]`&lt;P*#XS&gt;+]48:J6C(\AZ&gt;!9R/W5%KAF&amp;,S3*J!UWI+V@R"7,,,DT4.REV3KWA4[C;3NEALEI[V;"W26KNN&amp;:GCRNJKD)[L&amp;N"7B/J3ROFUR'L`K75IO_OZ^`X9^`:X@JEQQZW&gt;Z:ZT\TXH]TXX"QN1N9QP-U4BQ1AQ`0PY98-%#LUB"G#MGA0Z4_51]&amp;O9$Y'RW*A)L/?W]/=.57:B"%T?5#8H&amp;I@B0?Q&gt;OR$\#BRG'0YN\$K0N['TQAA5?U08GJO%-#_]M&amp;!9TF?]GG%2@Y#*'DYJW$`A$A4^/#!%FZ,78-V%A2'&gt;,"NUX?8J]A5&amp;]KWRGL.2F]9)]'*I55!),U'0/01PK5NG'RRF`)J,1*&gt;,Y@DRYX%DMW253;&gt;23WS/INM4T,9-.C6IUS/%L[=WB&gt;1'RTGBD#0;IY@)X)F2IGGJ'/,2&amp;/U?5*:]6"F,N%@75\O,&amp;S_C(&lt;;SX?Y)7)4Q"M\'P?5^FT``Y]'8A!%G?#KW'&gt;02RM7=U8&amp;W/8P*X%RS13V+-"U6;RC_"4_X2+!K'$*M"V9?E2G'1T1:L*+-N:A-JJYGQSA/]R$0RK%MW2$:-&gt;?COM[_HFZ@Q/(@[7DP^04U/,I$?_\X^0I=8E_P*TF0NYABYSI3!T)9213MQ$+$U+7.O2_/(4O'9=!W&lt;LI/4;]6QKK&gt;G;YGI-&lt;&gt;'Y]\'45?PVMR@O,FOX?2'&amp;;M-3DE=J4=)\2VU\;.NBWU\;0N!'U@I?X7//0,E8%TH\A!(?-LJZ`RGR#^"R-9BQ[G'`:GY(76:"2H(0M$MQ][-ND5IEW`FH'U[5#&lt;P&gt;E:8ZX%/,&amp;.9(RE:%2HBTF;IT,/-IT+_.896=,YM\&amp;`-K&gt;A0W7=IR:&lt;-"VCS!&amp;S#JA6/%Y&lt;N/L*XJ;3&lt;!:TU*IZ"YZZ).&amp;&gt;8"`Q&gt;`73&lt;(2OP.`8V:O=E_VCC&amp;MJB"E=ALAPDHU)S_#DSHL`M3IZO,MF"2BFB/&amp;[M+JB`6@+M'*Y^F#%Z__6%-YD#-PSPN=&lt;WM[T7EUMAA0[/"S+I[D%)@9@3K-R+YO##Z)8`65R:&amp;OB&amp;T','O"A/+O)$[.JK5&lt;%*#%GH("7%9`I2-QK)G;J.*_A\1H;PEL&lt;UX'R^B+R7B)HKB.L``3,&gt;6]KM1YS,)RE%.ZQEFB:D)]6"D090*QE6B&lt;(M3LD:",LI]FC::7R-IHV]1RCD4Y4OUL_5L'SCFC@%%.O2;SURTOQD[;A1%H"VUA+.K13[W$7()SZ,,@8X6&amp;8[\ZR22XK.?"0J^9HR6$:$:*;Z42&lt;)!^OB-7K9&amp;=G"`DLEJ%H&lt;F1/CW%NV+6,3F%%HJ)5INWLY$&lt;9L%W$W9\,.`!W6@IWZ8/-ZWR'?XQ#XU"H._PFNA$S=19.SL2DZH1J`C:/@L51NM5H&lt;Y[^$7Y=7*\]P^/6H+?FEN-O#3Z@+4H?#(Q,M6GLT-)."WEC]Z2%@FP2EPC#P5EJ._^"%?&lt;S9)ZF.Z&gt;MYAS03D0=*(^T4Q3_YQWR0)BB?Z-CY9`!QXL/HC(4;^981WFWFEE5QQM2?"&lt;(N/&amp;[#T8A&amp;M#D5!:0*;4HT*ET'/1L6[Z)(I./''!.19&amp;]9[Q/^F&lt;5O(@+&lt;O&gt;(Y,O9^/6#W+2*?DY59]C(=P$++F\(;P,:FFZ`N_T8&amp;)(PI&gt;_&lt;B,#"_H7LJTU4G.$8[/AI_M)W$G??9]Q?\_+A8:SEZ6USO?W=3W)XM3OJ(%\3[LOK;/.MHJ.7G;?:T45U;Z/?T9\=:\.$.RM5S`&gt;R.NI4*I.=%)$UGR-.O(ZT_A'370%JK'A&amp;X+*]EG,G61Y:+BV1[9R6OM"]#6=!REP30R7AG!9NL_^`M@4NAVS,\!;LZ!FNF8Q(#_JVY+$UZCPUPJ3W3FL5MHUY(&lt;[C[][OZ*JY5CJP,@)*JBQ-M!Z='7L&lt;4V,7.G&amp;SN?WH[7K&lt;05NNQ[D`4.,`X6*1/;6#X2G"56+BN0\7A5V@J6YGA&gt;R%!WF6SM#\!,%IV/3QX22M]0M\@2YZGHS&amp;&gt;0@#@Q&gt;B+904K6!$D)T`!J@)#W'LZMA$3]'3&amp;#^74SXJYK"&gt;H+36K"8M0U`G/UX0FX0O/4KRHGL_-!?PU"S9FEAZ-')/BL929SX7LWKQDFW(7(&gt;!L2\L8[8&amp;?GVWL-&gt;=B86^A1"O%3W_&amp;(P_W%4Z`P6U]DU_"&lt;Z`I_/\5/(\LAD]VBO;JQ@]9\"/P]`^,M7B`VUI1-"L=Q']O?]_8W"0?`*W&amp;S,&lt;X8*FOZ-G9!!DF-)KR?^`^1N+W/XQ'\L&lt;,7\&lt;&amp;9`Z\[6^1.K6SN9L-CG%AF1"DX6L!N[J#4B[_I.W2\%-%%_&amp;[-G;RJ.@Y_F?P;=`IC?]FB:,!?3ESY-2?#D7?-J8L#_\YZYO,^.]LN&amp;]LIZ``O#U(J48ECZ'"4DJV&lt;J=J9,E&amp;)7E[+1%32&amp;O/O-/=K+B@];&gt;8,`SU;5_X7GV?6KLT1\5ZH.QMV[&lt;@UKLT6NTU7&lt;26H^HLW?8TZ&amp;3H(_?K$B@HUZRPD%&amp;=@Z&amp;*U[42JRHMYPTX!S*]`S-C00.;20HX[:.H(``PYIT0%FR8N#*=X[OYD1W;R4!H2`PY[)H]=[-@V8JYC(H%8,8.[!56U#6`M(H!(HY;N!&gt;&lt;O4XD.+=&lt;G!J$IJYDPGS'$+7;-YR#`$K8!.,=/8FG^!.;6/&gt;9]J1H1\;R5F;^@4&gt;E(TIE,J[9[1LNNG\4NWL^D*S5(K:OY&gt;'46JD+&gt;L@!&amp;8;*[F5;ST(,P*JC2Y)F3']S&lt;/:B;\;]^JDGJP+*[Q3F^&gt;)JT;-E^Q0C^A!&amp;D(91%*Q&amp;AET1*/_A$W5]GGV#!FLSO%`$9RVOTV&gt;8&lt;\/:H^S#@MCFD"D1AGTYM7X%GZ*+-JYQ=XR/IWZ$7*O%[[^0)[]=(;OP9-YGY2L&lt;TH;6]X#N2&gt;L[:#OFLY3D\A6:`08=W`C&lt;0K'HJ\?CPKFJ0`-+=,FXZ&lt;^(8C`&amp;O!C#?"C,+R$DA(M._4MD]8)B4N/=J^+]P09Y3RU[%H_4&amp;K3/X*]L3K7&lt;R^X_$Q^@1'@.ZHJ"[;.[@C_D"2^&gt;EYR`&lt;EZR@4?77$[]Z.F_AMJG/:T/CSIF(?IF!@R__?B8E^Z:VL+[X/E00U&gt;'Q'`&lt;W9!\ZJ4A0PH&amp;/$&gt;MQ$YJS=,?#!&amp;Y/;*!&gt;[G!NZ+'%UMYZ[JFX(ZIJK_D/_9'=L&lt;ZR4FXDF&amp;O7]7+.]Z7=JXJ;#]:'+5OV8+X@B^+T4K+2@45N[9)_5:HG/1])UT1XD^H#+]95Y2@PMM%.YY7=+&lt;5B"?/A(#NU4AC/:H/`28+'ZIU$]YPJDW:TM.72Y=6=J.^(=\X?2X/VM&lt;EV]&gt;@SC'O!G^/PZ)`MX/N,Q[(J`#K_/0&gt;;_/&amp;LSYJXIN5K0?)ICR4"XYD?CW0Q*7M;\E.&lt;Y26W$C'LG.`"&amp;$F'%CM)!\QLV@)4*D&lt;[A`Z"T&lt;L"I(4U%$')1H\6T:`Q!^8]WG!!!!!!!!"!!!!,Y!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!)"!!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!#'R9!A!!!!!!"!!A!-0````]!!1!!!!!"`Q!!!")!"!!!!&amp;1!]!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!!1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!!21!I!#V:P&lt;(2B:W5A5W6U!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!&amp;5!+!!^$&gt;8*S:7ZU476B=X6S:71!$U!$!!F$;'&amp;O&lt;G6M4G]!#%!B!E^O!!"&amp;!0(6C,!4!!!!!26$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'Q!*U!7!!%%5X2P=!!!&amp;%B$45-Y-$1T1W^O&gt;(*P&lt;%6W:7ZU!!!A1(!!'1!"!!E53%..1TAQ.$.$&lt;WZU=G^M28:F&lt;H1!!!1!)1!31(!!#!!"!!M!#!!!!E^O!!!&amp;!!I!!"J!=!!)!!%!$1!3!!!+1X6S=G6O&gt;&amp;.F&gt;!!!'E"Q!!A!!1!.!")!!!N7&lt;WRU97&gt;F)&amp;.F&gt;!![1(!!&amp;Q!!!!1!!1!!!_A!#A!"$'9]!!!-!!%-:DQ!!!Y!!1RG0!!!$Q^'=G^O&gt;&amp;"B&lt;G6M28:F&lt;H1!-E"1!!A!!A!$!!1!"1!'!!=!#!!1'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!"!"%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!229!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!#!!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$6D7Y'!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E7!)!!!!!!!1!&amp;!!=!!!%!!.7.&lt;A9!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!#'R9!A!!!!!!"!!A!-0````]!!1!!!!!"`Q!!!")!"!!!!&amp;1!]!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!!1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!!21!I!#V:P&lt;(2B:W5A5W6U!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!&amp;5!+!!^$&gt;8*S:7ZU476B=X6S:71!$U!$!!F$;'&amp;O&lt;G6M4G]!#%!B!E^O!!"&amp;!0(6C,!4!!!!!26$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'Q!*U!7!!%%5X2P=!!!&amp;%B$45-Y-$1T1W^O&gt;(*P&lt;%6W:7ZU!!!A1(!!'1!"!!E53%..1TAQ.$.$&lt;WZU=G^M28:F&lt;H1!!!1!)1!31(!!#!!"!!M!#!!!!E^O!!!&amp;!!I!!"J!=!!)!!%!$1!3!!!+1X6S=G6O&gt;&amp;.F&gt;!!!'E"Q!!A!!1!.!")!!!N7&lt;WRU97&gt;F)&amp;.F&gt;!![1(!!&amp;Q!!!!1!!1!!!_A!#A!"$'9]!!!-!!%-:DQ!!!Y!!1RG0!!!$Q^'=G^O&gt;&amp;"B&lt;G6M28:F&lt;H1!-E"1!!A!!A!$!!1!"1!'!!=!#!!1'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!"!"%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E7!)!!!!!!!1!&amp;!!-!!!%!!!!!!$A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!#."9!A!!!!!!3!!1!!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!%!!A!!$%:S&lt;WZU='&amp;O:7R731!!%5!+!!N7&lt;WRU97&gt;F)&amp;.F&gt;!!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!!21!I!#E.V=H*F&lt;H24:81!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!!^!!Q!*1WBB&lt;GZF&lt;%ZP!!B!)1*0&lt;A!!21$RV9CQ%Q!!!!%61W^O&gt;(*P&lt;%6W:7ZU6(FQ:8-O9X2M!#&gt;!&amp;A!""&amp;.U&lt;X!!!"2)1UV$/$!U-U.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;!!!)%"Q!"E!!1!*&amp;%B$45-Y-$1T1W^O&gt;(*P&lt;%6W:7ZU!!!%!#%!%E"Q!!A!!1!,!!A!!!*0&lt;A!!"1!+!!!;1(!!#!!"!!U!%A!!#E.V=H*F&lt;H24:81!!"J!=!!)!!%!$1!3!!!,6G^M&gt;'&amp;H:3"4:81!/E"Q!"=!!!!%!!%!!!0I!!I!!1RG0!!!$!!"$'9]!!!/!!%-:DQ!!!]02H*P&lt;H2197ZF&lt;%6W:7ZU!$*!5!!)!!)!!Q!%!!5!"A!(!!A!%"J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!!1!2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!&amp;!!2!!!!"!!!!/!!!!!I!!!!!A!!"!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!F!!!!5B?*S65]NOUV!10;HT&lt;N+%P'B$7RQ+F0))3/WC1CQM,#K1;)F)V46O=F-C'&gt;NS\.$M7,*BQ:`Q-`Q$3`A$TL7&gt;.)&amp;'44/+(T.T:][=-Q&lt;Q&amp;/@Y]`0LDT)!:&lt;.F@R:OWX==RRTJJD%9P/W@OI9\;JJ$MX^;HI2.[?H)B+JO7ZZLG[_'QP+/2YY9.$O?C7WNBFC]\&gt;E/5(GN(_L\T`:WJV/"/$M?YT&gt;SC-UR%.PV@_O;AT20,^'1/X$:V$%M9:[]!7ZI73S@W+:HH!GV,4R5[3B%DE.B$(R8&gt;)/ML/[\,H(+J$!L=ESS#JK#D0\2M&amp;D\S%:;;SS^MQ#6\&gt;=E_,FD.V#+)#\TCO"5!FGA(LHT+'%7Q%RE:I$HD.S%,-O7SC_7C?6[,S":$?YLU&lt;V1#*BI33:#*._J_T=LO9$O^3H&gt;IY((]N=O#5H^^\57!3O%F5!3+4ZH5.T148`A#6?V?WJQ7H8=`N$QB.IV0)/:#9[.,A'N=MY%DY?PW%5&amp;'3U&amp;J7??E?APCPX?#RX#\K()8AJKJ'&amp;6]&lt;M^DDSV!(C)"1ECS2*TH33(6K%_NR4PP-MT,&gt;:;Q`JEO7+8,.@7V4M`8I\&lt;=_,`KKEOL/:?!(ZTBP$'F91P)5DHB@X3_"#U0O#U+L:QFS*)OU=GRF;+\(`02?2_9+&amp;S-7R(U+[X#VFU)"'F/8+,*\/E9)5;Z8("-8*;)`83NEVB7-DT%UU&gt;_:_%W_]1/4^VF&amp;(&amp;"O\A!8;Y"1L`]&lt;!X5?X1CHD%OI^F)4RB^3&lt;L.RF0%W/37/.1`A+]"0,C!!!!&gt;Q!"!!)!!Q!&amp;!!!!7!!0"!!!!!!0!.A!V1!!!'%!$Q1!!!!!$Q$9!.5!!!"K!!]%!!!!!!]!W!$6!!!!=Y!!B!#!!!!0!.A!V1!!!(7!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!%R!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!#!Y!!!%(Q!!!#!!!#!9!!!!!!!!!!!!!!!A!!!!.!!!"!A!!!!&lt;4%F#4A!!!!!!!!&amp;54&amp;:45A!!!!!!!!&amp;I5F242Q!!!!!!!!&amp;]1U.46!!!!!!!!!'14%FW;1!!!!!!!!'E1U^/5!!!!!!!!!'Y6%UY-!!!!!!!!!(-2%:%5Q!!!!!!!!(A4%FE=Q!!!!!!!!(U6EF$2!!!!!!!!!))&gt;G6S=Q!!!!1!!!)=5U.45A!!!!!!!!+!2U.15A!!!!!!!!+535.04A!!!!!!!!+I;7.M/!!!!!!!!!+]4%FG=!!!!!!!!!,12F")9A!!!!!!!!,E2F"421!!!!!!!!,Y6F"%5!!!!!!!!!--4%FC:!!!!!!!!!-A1E2)9A!!!!!!!!-U1E2421!!!!!!!!.)6EF55Q!!!!!!!!.=2&amp;2)5!!!!!!!!!.Q466*2!!!!!!!!!/%3%F46!!!!!!!!!/96E.55!!!!!!!!!/M2F2"1A!!!!!!!!0!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"%!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!`!!!!!!!!!!!`````Q!!!!!!!!%%!!!!!!!!!!$`````!!!!!!!!!=!!!!!!!!!!!0````]!!!!!!!!"S!!!!!!!!!!!`````Q!!!!!!!!(Y!!!!!!!!!!$`````!!!!!!!!!E1!!!!!!!!!!0````]!!!!!!!!#6!!!!!!!!!!%`````Q!!!!!!!!3M!!!!!!!!!!@`````!!!!!!!!",Q!!!!!!!!!#0````]!!!!!!!!%T!!!!!!!!!!*`````Q!!!!!!!!4=!!!!!!!!!!L`````!!!!!!!!"/Q!!!!!!!!!!0````]!!!!!!!!%`!!!!!!!!!!!`````Q!!!!!!!!55!!!!!!!!!!$`````!!!!!!!!"3A!!!!!!!!!!0````]!!!!!!!!&amp;L!!!!!!!!!!!`````Q!!!!!!!!GQ!!!!!!!!!!$`````!!!!!!!!#IA!!!!!!!!!!0````]!!!!!!!!5&gt;!!!!!!!!!!!`````Q!!!!!!!"2]!!!!!!!!!!$`````!!!!!!!!&amp;)1!!!!!!!!!!0````]!!!!!!!!5F!!!!!!!!!!!`````Q!!!!!!!"4]!!!!!!!!!!$`````!!!!!!!!&amp;11!!!!!!!!!!0````]!!!!!!!!&gt;$!!!!!!!!!!!`````Q!!!!!!!"U5!!!!!!!!!!$`````!!!!!!!!(2Q!!!!!!!!!!0````]!!!!!!!!&gt;3!!!!!!!!!#!`````Q!!!!!!!"_=!!!!!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!BZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!!!!9!!1!!!!!!!!%!!!!"!#*!5!!!'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!1!!!!1!"!!!!&amp;1!]!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!!1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!"E!0(6C'8E!!!!!BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=R:1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO9X2M!#J!5!!"!!)&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!$!!!!!@````]!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!!!!!!!!!%!!!!!!!)"!!!!#A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(%"Q!!A!!1!"!!)!!!R'=G^O&gt;("B&lt;G6M6EE!!"&amp;!#A!,6G^M&gt;'&amp;H:3"4:81!&amp;5!+!!^7&lt;WRU97&gt;F476B=X6S:71!%5!+!!J$&gt;8*S:7ZU5W6U!!!61!I!$U.V=H*F&lt;H2.:7&amp;T&gt;8*F:!!01!I!#5.I97ZO:7R/&lt;Q!)1#%#4WY!!(!!]&gt;7):X9!!!!#'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZD&gt;'Q!.E"1!!=!!A!$!!1!"1!'!!=!#"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!E!!!!(!!!!!0```````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!$!1!!!!I!"!!!!&amp;1!]!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!!1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!!21!I!#V:P&lt;(2B:W5A5W6U!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!&amp;5!+!!^$&gt;8*S:7ZU476B=X6S:71!$U!$!!F$;'&amp;O&lt;G6M4G]!#%!B!E^O!!"Q!0(6C(01!!!!!BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=R:1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO9X2M!$:!5!!(!!)!!Q!%!!5!"A!(!!A&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!*!!!!"Q!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!%!1!!!")!"!!!!&amp;1!]!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!!1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!!21!I!#V:P&lt;(2B:W5A5W6U!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!&amp;5!+!!^$&gt;8*S:7ZU476B=X6S:71!$U!$!!F$;'&amp;O&lt;G6M4G]!#%!B!E^O!!"&amp;!0(6C,!4!!!!!26$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'Q!*U!7!!%%5X2P=!!!&amp;%B$45-Y-$1T1W^O&gt;(*P&lt;%6W:7ZU!!!A1(!!'1!"!!E53%..1TAQ.$.$&lt;WZU=G^M28:F&lt;H1!!!1!)1!31(!!#!!"!!M!#!!!!E^O!!!&amp;!!I!!"J!=!!)!!%!$1!3!!!+1X6S=G6O&gt;&amp;.F&gt;!!!'E"Q!!A!!1!.!")!!!N7&lt;WRU97&gt;F)&amp;.F&gt;!![1(!!&amp;Q!!!!1!!1!!!_A!#A!"$'9]!!!-!!%-:DQ!!!Y!!1RG0!!!$Q^'=G^O&gt;&amp;"B&lt;G6M28:F&lt;H1!=A$RV9CYI1!!!!);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,G.U&lt;!!Y1&amp;!!#!!#!!-!"!!&amp;!!9!"Q!)!"!&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!2!!!!#!!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!&lt;`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;A#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!%A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(%"Q!!A!!1!"!!)!!!R'=G^O&gt;("B&lt;G6M6EE!!"&amp;!#A!,6G^M&gt;'&amp;H:3"4:81!&amp;5!+!!^7&lt;WRU97&gt;F476B=X6S:71!%5!+!!J$&gt;8*S:7ZU5W6U!!!61!I!$U.V=H*F&lt;H2.:7&amp;T&gt;8*F:!!01!-!#5.I97ZO:7R/&lt;Q!)1#%#4WY!!%5!]&gt;7)M"-!!!!"&amp;5.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;&amp;2Z='6T,G.U&lt;!!H1"9!!124&gt;'^Q!!!53%..1TAQ.$.$&lt;WZU=G^M28:F&lt;H1!!#"!=!!:!!%!#22)1UV$/$!U-U.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;!!!"!!B!"*!=!!)!!%!#Q!)!!!#4WY!!!5!#A!!'E"Q!!A!!1!.!")!!!J$&gt;8*S:7ZU5W6U!!!;1(!!#!!"!!U!%A!!#V:P&lt;(2B:W5A5W6U!$J!=!!8!!!!"!!"!!!$[!!+!!%-:DQ!!!Q!!1RG0!!!$A!"$'9]!!!0$U:S&lt;WZU5'&amp;O:7R&amp;&gt;G6O&gt;!"S!0(6C,CB!!!!!BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=R:1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO9X2M!$B!5!!)!!)!!Q!%!!5!"A!(!!A!%"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!"%!!!!"`````A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!"!!!!'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="PowerSupplyChannel.ctl" Type="Class Private Data" URL="PowerSupplyChannel.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="FrontPanel" Type="Folder">
		<Item Name="DefaultFP.vi" Type="VI" URL="../DefaultFP.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
	</Item>
	<Item Name="Properties" Type="Folder">
		<Item Name="ChannelNo" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ChannelNo</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ChannelNo</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read ChannelNo.vi" Type="VI" URL="../Properties/Read ChannelNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!!Q!*1WBB&lt;GZF&lt;%ZP!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write ChannelNo.vi" Type="VI" URL="../Properties/Write ChannelNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!01!-!#5.I97ZO:7R/&lt;Q";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="CurrentMeasured" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">CurrentMeasured</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">CurrentMeasured</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read CurrentMeasured.vi" Type="VI" URL="../Properties/Read CurrentMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write CurrentMeasured.vi" Type="VI" URL="../Properties/Write CurrentMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$U.V=H*F&lt;H2.:7&amp;T&gt;8*F:!";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="CurrentSet" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">CurrentSet</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">CurrentSet</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read CurrentSet.vi" Type="VI" URL="../Properties/Read CurrentSet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!8%"Q!"Y!!$M?5'^X:8*4&gt;8"Q='RZ1WRB=X.-;7*S98*Z,GRW&lt;'FC'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write CurrentSet.vi" Type="VI" URL="../Properties/Write CurrentSet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#E.V=H*F&lt;H24:81!!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="FrontPanel" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">FrontPanel</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">FrontPanel</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read FrontpanelVI.vi" Type="VI" URL="../Properties/Read FrontpanelVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(D!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!1!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!"1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!"=1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$M?5'^X:8*4&gt;8"Q='RZ1WRB=X.-;7*S98*Z,GRW&lt;'FC'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!"61&lt;X&gt;F=F.V=("M?5.I97ZO:7QA;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write FrontpanelVI.vi" Type="VI" URL="../Properties/Write FrontpanelVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(D!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"5!0!!$!!%!!1!"!!%!!1!"!!%!!1!"!!%!!1!"!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!=!!A!!$%:S&lt;WZU='&amp;O:7R731!!7E"Q!"Y!!$M?5'^X:8*4&gt;8"Q='RZ1WRB=X.-;7*S98*Z,GRW&lt;'FC'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!"61&lt;X&gt;F=F.V=("M?5.I97ZO:7QA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*)!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="On" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">On</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">On</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read On.vi" Type="VI" URL="../Properties/Read On.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;\!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!B!)1*0&lt;A!!8%"Q!"Y!!$M?5'^X:8*4&gt;8"Q='RZ1WRB=X.-;7*S98*Z,GRW&lt;'FC'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write On.vi" Type="VI" URL="../Properties/Write On.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;\!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!)1#%#4WY!!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="Voltage Set" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Voltage Set</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Voltage Set</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Voltage Set.vi" Type="VI" URL="../Properties/Read Voltage Set.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!,6G^M&gt;'&amp;H:3"4:81!8%"Q!"Y!!$M?5'^X:8*4&gt;8"Q='RZ1WRB=X.-;7*S98*Z,GRW&lt;'FC'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write Voltage Set.vi" Type="VI" URL="../Properties/Write Voltage Set.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#V:P&lt;(2B:W5A5W6U!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="VoltageMeasured" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VoltageMeasured</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VoltageMeasured</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read VoltageMeasured.vi" Type="VI" URL="../Properties/Read VoltageMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write VoltageMeasured.vi" Type="VI" URL="../Properties/Write VoltageMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!')!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%8!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!7E"Q!"Y!!$M?5'^X:8*4&gt;8"Q='RZ1WRB=X.-;7*S98*Z,GRW&lt;'FC'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!"61&lt;X&gt;F=F.V=("M?5.I97ZO:7QA;7Y!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!9$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!(!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Configure.vi" Type="VI" URL="../Configure.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!I!$%.V=H*F&lt;H2-;7VJ&gt;!!!$5!+!!&gt;7&lt;WRU97&gt;F!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!#3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
	</Item>
	<Item Name="Init.vi" Type="VI" URL="../Init.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;T!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
	</Item>
	<Item Name="Measure.vi" Type="VI" URL="../Measure.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'.!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!+!!&gt;7&lt;WRU97&gt;F!!V!#A!(1X6S=G6O&gt;!"=1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!"Q!*!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
	</Item>
	<Item Name="Set.vi" Type="VI" URL="../Set.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&amp;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;R!=!!?!!!\(F"P&gt;W6S5X6Q=("M?5.M98.T4'FC=G&amp;S?3ZM&gt;GRJ9BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31#%-1WBB&lt;GZF&lt;&amp;.U982F!!";1(!!(A!!/RZ1&lt;X&gt;F=F.V=("Q&lt;(F$&lt;'&amp;T=URJ9H*B=HEO&lt;(:M;7);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
	</Item>
</LVClass>
