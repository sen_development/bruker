﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="HCMC8043ClassLibrary.lvlib" Type="Library" URL="../HCMC8043ClassLibrary.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Close.vi" Type="VI" URL="/&lt;instrlib&gt;/Rohde&amp;Schwarz HMC 804x Power Supplies/Public/Close.vi"/>
				<Item Name="Configure Output Enabled.vi" Type="VI" URL="/&lt;instrlib&gt;/Rohde&amp;Schwarz HMC 804x Power Supplies/Public/Configuration/Configure Output Enabled.vi"/>
				<Item Name="Configure Output.vi" Type="VI" URL="/&lt;instrlib&gt;/Rohde&amp;Schwarz HMC 804x Power Supplies/Public/Configuration/Configure Output.vi"/>
				<Item Name="Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/Rohde&amp;Schwarz HMC 804x Power Supplies/Public/Initialize.vi"/>
				<Item Name="Measure.vi" Type="VI" URL="/&lt;instrlib&gt;/Rohde&amp;Schwarz HMC 804x Power Supplies/Public/Action/Measure.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Fit VI window to Largest Dec__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Fit VI window to Largest Dec__ogtk.vi"/>
			</Item>
			<Item Name="PowerSuppplyClassLibrary.lvlib" Type="Library" URL="../../PowerSupplyClassLibrary/PowerSuppplyClassLibrary.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
