﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.3</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"HG5F.31QU+!!.-6E.$4%*76Q!!&amp;8A!!!2O!!!!)!!!&amp;6A!!!!3!!!!!1V3:7RB?3ZM&gt;G.M98.T!!!!!!#A&amp;A#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!!\B/%,R&lt;LJ-L&gt;Z=W[+;25-!!!!-!!!!%!!!!!##BRP9WV:&gt;4&lt;1+&amp;C*08".NV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!'BI([!`2TUOPOXFB9]_Q=!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"!@4=H,J;&amp;=!&lt;V,X-TSI=__!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)1!!!"BYH'.A9W"K9,D!!-3-$EQ.4"F!VA?'!!9!0[%&amp;FA!!!!!!!")!!!!-?*RD9'(AA%)'!!$Y!#5!!!!!!%E!!!%9?*RD9-!%`Y%!3$%S-$#^!N)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!VAB!JJJN!@!,&gt;((YI@1&amp;*$!#I,SER!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!:-!!!-Y?*R&lt;Q-D!E'FM9@9"3$-$M2B$!U.S@EIK,Q/1TQ!"&lt;ZA93!9"50V;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;;JZ-JV!73!NA;1"&lt;C#+`1&amp;!62Q.&amp;=I-*3S'"[)/(W]Q991Y&amp;/;%+'TO*&gt;\]ZD==1%]*((T)UNWI!?4X4A321#'?TB!/C?-O($JC1$\D#:#"H4QQ8X0!`2-'-K"%2;$4"'12#S0-IG[WYQY;Y("Q%)&amp;1'2#K!E)6A+A&gt;9"==Y9A\$!``N;`P\7)&amp;UMB2;A$%)06[$)Q-D'!Z2I;V5$E&lt;)*M*+A;,3R"&lt;!2J-D!TW=$WXI@);3/;Y--,U).26)^H,"$;$E?%0!]Q]I(V103!XM5(&amp;@)&amp;C"[$M%#"\!J1&gt;$72`A,+4A'Q"+$M4S$:AB,$TI'RH@R&gt;8Z,1)3N?Q.!Y+R/4=!A-$P7IHH6)@+!KO&amp;1;*&amp;S38I1ET!!$K9*@`!!!!!2!!!!&amp;E?*QT9'"AS$3W-'.A:'"A"G)RBA;'Z0S56!9E9-+)9!?(BT7`%?BW56(I&gt;&amp;(B[@:25?HU5?&amp;IM'4AHXKIQ1*)(O[Y:X$.Y()*.`_W9]U@'&amp;^W!$8V"D"WBKDQ^"9S&gt;J;I=(1\-&amp;I\-P*0/6#;V/P%!D+GNY[FMQ9IY=6C\=@#0_6AK6_P"QP18*\?=";A2K!/&amp;GNX&amp;J!/KVZ(C![Q2IZO2R;A*K"+FEY7F2&gt;``P``X`Q$;0*"`GUH_6V/&gt;*ZYT=O`\="L$PZN"V]T!VH.2Q4CYK-0)`NN\?N\OU$?1`)CAQ%1+T&amp;)A-79A(A/EDA)/0O\O++($UCN#"!HZR99'/B6/_G5_E"2=#U!?CF7Q!!!!,-!!!%U?*QT9'"AS$3W-,M!J*E:'2D%'"I9EP.45BG1!!MDANX]2K$&lt;257EUU7&amp;J^N(2;,42Y7DC['$%3D'U_P"#/3#B$F[!RE\1V1YOBU9/RWB=CQ)/2;)(%OH)QM*=MU(15SQH)]+3S?,SIM`````&lt;TX!\X+A_9D!9;D\VL[_NQPE8#1H-RA!=2:1"#4'"-1^3/)AY/TPYILO8Z";93"/,EAOU[NWUCHVA;,A7A9!:-U`,A!!!!!-&amp;A#!'1!!"$%W,D!!!!!!$"9!A!!!!!1R.CYQ!!!!!!Q7!)!:!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"E!!!1R.CYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!M,!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!ON8YGN#Q!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!ON8T5V.47*L1M!!!!!!!!!!!!!!!!!!!!!``]!!!ON8T5V.45V.45VC;U,!!!!!!!!!!!!!!!!!!$``Q#*8T5V.45V.45V.45V.9GN!!!!!!!!!!!!!!!!!0``!&amp;^@.45V.45V.45V.45V`IE!!!!!!!!!!!!!!!!!``]!8YG*8T5V.45V.45V`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C6]V.45V`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9F@L@\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!)G*C9G*C9G*`P\_`P\_C9E!!!!!!!!!!!!!!!!!``]!!&amp;^@C9G*C9H_`P\_C;V@!!!!!!!!!!!!!!!!!!$``Q!!!!"@C9G*C@\_C9F@!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!8YG*C9EV!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!&amp;]V!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!#!!%!!!!!!!Q!!5:13&amp;!!!!!!!!-!!!6Y!!!3/XC=T:B&gt;4"R6&amp;-@0H:WFMU"FFA^B'TZW.\-L;9#3*J:+L&amp;)9I,5%L&lt;17U!A,OR938"L9L39G2:-.#1]E4;LYE&gt;11(XDVA43_].!9CC:DN%7NM48:QCMR-3:.#4+MZ^\:W:H^9%(5FHWYO&gt;G=`THXH0-\MX-8I,"",//WY)I+2(S)GQY6=PU+!6CM&amp;3$_]5[#?*\]";49162I%M[,^\EN5KZ#PF`R#P8S&amp;0S"VL%(M3]MF`D0R25UT2%&gt;[#R8B1+`5GJ`39K+UJ@FUJ26^VI%X?)UW?+[J;J(QH2E&amp;!.#J*KO^FKS"52W]@SC_[GW1(!]&amp;"A&lt;#)Q&amp;BA?()B)VM.5+$O&lt;&gt;JI)I+Q@(J/AT["R0]48TTK^:*PB.X4OA^WK9HZ]X2(:.Z'5H/IY;SQ3!Z3K`FE64C*JR+6L$.,F-AX'O[H(EKKVL.!UK3J5731IK5@:O0(EKV76K%Z/NLK[C$.?Y&lt;%C&amp;9CH;,$C%&amp;@]^[]'TE;_!!)H=BEJM4*]1=WUN78T]GLW4&gt;I5J#L%RHE9C&gt;O'_3Y8$%98L"VYP^S&lt;=9'XB^&lt;9]DWUB&lt;;QN.HF+"+-P.X&lt;ICYR^K7Q:#&gt;/_/(XB=??:%&gt;`Y?#"Y;7TYMC`E^Y5#Q@1_H:!67Q-N!IX';)&amp;3Y'%&gt;BMQV(Y7ZO4GM![['^!75FEL2B+[)J4/3K,P@K$O.;B4Q23SAP(\O)CWCJZ(4)&lt;9SC$P9/MV7B;[EE?X&lt;$&lt;C0)NRW-@7Y38!`_\`#@5R7+K[EQ!V_7)&amp;Q&amp;F!&lt;.*%".^I$`!H_,*LDK*EQQYU;0WL#/]0^H+S)S824&lt;1L&gt;-T-T34JM4G/#&lt;JY1H?\9:GS4UPV&gt;&lt;)V5Q!?-&lt;CN4H-8/50_T[0^\^*`5B&gt;?W:&gt;G&lt;P1X/(+!]2^S&gt;PL=$+@805_'#L$DQH!7;7Y'[0915?#%8=`LVXG_95XBSVF[FT:77];.PK4[_`];U8T,N&lt;QE/7Z52JVO,=VG,=V/H\4#,AR(_ETB9JR[=J+.3.-]UB!Y7JV`P6]T/_L7RM9%R=&gt;8K&amp;(("_TQ@E?AXIE&gt;\YO$X)&amp;54&lt;+((D'RPSL#79%HL$"[W(^&lt;8E1&gt;0$XB[!5@WFO=^4JN;CX@3%D&gt;["5@5L_2!-`5&lt;1P%MS)S$!`(]3&amp;.'$P+1!XF8(#S[B;ZXBE/$1ZWD+4!]5-F*P],4[O5G0=)+I"J/J&amp;2P?8EZL8J=FOJ&amp;1J[[_L@CE@*6UIQV0#:&amp;/2;IHG62C)?JB(RUPL#QA-ZR:4BQV*8&amp;O6CF!:_0*EZGYK+L[)Z8@6"Q;V3EGN,"&gt;.%VW41"$:[G290'9DK.%`5V?T`.Q/Z0-Z"UGDS6S%ED?&gt;.I1EH;3&amp;LV-6CP.U:C`9BJ8W@;VZL'ZIYJJEUFL7E`1(G9`ME&gt;G;9I*JB_.6]$GP&gt;/=FYH?&amp;URLRM3:).#S?;I^V0I)A3&gt;374$H7X*\NQFW&lt;;7)6]Q'"D*A$9M0S[UY=@^B$&lt;]N*`1BJ]@0^JQ&gt;Y^IQS]:U,:G2HM[A89ZODC&amp;XJ01PL9NWP5\I-X&amp;U&lt;;WD);$I83M0^QLVH2K`D(;(_ULN'@W&amp;&gt;I@0Q'U0^ELWJ^G1$MH!^IPYS6#P`H_DB\+I3`ZCN60S7ZH:*=9:&amp;P!$LW\@'A8;+]DQ]',83&amp;@+$SO-:\N&amp;1R&lt;\]0%#[6IC:ZY'9;MA4=QW=IT')?ORKOEU=RC[-&amp;G5B-8820.&lt;%`POW&lt;KDV&amp;48(=W`@&gt;?T7Q0;(@*#[SMRGPG%?AT.T24DI@1R-F-8+S1\A1,;;&gt;Z!K;*("(#=W9)3T1)$_!.&gt;P*.7C?U\42&gt;ZY4\3_(%@T6#FS4(MBG)L2BC1I53O;8Q"`%U6CF@/#WUCN@RVIM8Y+?&amp;[]*$DUQ7\\)`&gt;%A&amp;+6\M3)ADN[%.//GT+K(M&lt;Q9&lt;`1A!!!!%!!!!4A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!-S!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!#.&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!"R!!!!"A!51#%05X&gt;J&gt;'.I;7ZH5X2B&gt;(6T!!N!"Q!&amp;1W^V&lt;H1!$U!(!!F$;'&amp;O&lt;G6M4G]!$U!(!!B4&gt;WFU9WB/&lt;Q!!$E!Q`````Q2/97VF!!!?1&amp;!!"1!!!!%!!A!$!!1.5G6M98EO&lt;(:D&lt;'&amp;T=Q!"!!5!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!/29!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!"1!!!!!!!!!"!!!!!A!!!!-!!!!%!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$34%X&lt;!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E7!)!!!!!!!1!&amp;!!=!!!%!!.*-4&gt;M!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!D29!A!!!!!!"!!A!-0````]!!1!!!!!!=1!!!!9!&amp;%!B$V.X;82D;'FO:V.U982V=Q!,1!=!"5.P&gt;7ZU!!^!"Q!*1WBB&lt;GZF&lt;%ZP!!^!"Q!)5X&gt;J&gt;'.I4G]!!!Z!-0````]%4G&amp;N:1!!(E"1!!5!!!!"!!)!!Q!%$6*F&lt;'&amp;Z,GRW9WRB=X-!!1!&amp;!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;A#!!!!!!!%!"1!$!!!"!!!!!!!2!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!)I7!)!!!!!!"A!51#%05X&gt;J&gt;'.I;7ZH5X2B&gt;(6T!!N!"Q!&amp;1W^V&lt;H1!$U!(!!F$;'&amp;O&lt;G6M4G]!$U!(!!B4&gt;WFU9WB/&lt;Q!!$E!Q`````Q2/97VF!!!?1&amp;!!"1!!!!%!!A!$!!1.5G6M98EO&lt;(:D&lt;'&amp;T=Q!"!!5!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"Q!-!!!!"!!!!59!!!!I!!!!!A!!"!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!3M!!!(&lt;?*S64VN/QT!1H-:JGV$[A*;XB.),6*8A!*%C]1.56=M"M"LX):E%.5["0]\),Z?!%]!Y+?+$,TS3V_/&gt;X:U&amp;=):?W'^0HV:GNFQFC[G2*M`1#/OI2GG?',4Z^+/F4"+F2WF"P6*/BF9Y`/*R2`*"!8@Y@,OZ@1@A.#&gt;+SZ?"XMSUT$+`:$/D=2G/5;7C!A=#\HGE]]SI&gt;3$T,,CW7J5]LF=&lt;;7*J6!*KK&gt;Z"T)I0V%D%FO)#(HT;%8/^A"?_CH2CSA_6TN(B(%&amp;F!\MCD_&gt;QU7&gt;R(3QK,(.P;[3&amp;&gt;I'/--]RZ70;;G)0QX`&lt;&gt;/R6MV@&amp;$LFHM9ML.AT1QQ%T&amp;I@-`+#WR&gt;_@X]R2A8,@#I\:%&gt;AP^P!:OTD";:&amp;R'&lt;O%3\//.@!.RD21E1!!!!"X!!%!!A!$!!5!!!"9!!]%!!!!!!]!W!$6!!!!91!0"!!!!!!0!.A!V1!!!'I!$Q1!!!!!$Q$9!.5!!!"T!"'%!)!!!"%!\Q$E!!!!&gt;9!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!4!!5F.31QU+!!.-6E.$4%*76Q!!&amp;8A!!!2O!!!!)!!!&amp;6A!!!!!!!!!!!!!!#!!!!!U!!!%:!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B$1V.5!!!!!!!!!:R-38:J!!!!!!!!!&lt;"$4UZ1!!!!!!!!!=2544AQ!!!!!1!!!&gt;B%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!A!!!CBW:8*T!!!!"!!!!G241V.3!!!!!!!!!MB(1V"3!!!!!!!!!NR*1U^/!!!!!!!!!P"J9WQY!!!!!!!!!Q2$5%-S!!!!!!!!!RB-37:Q!!!!!!!!!SR'5%BC!!!!!!!!!U"'5&amp;.&amp;!!!!!!!!!V275%21!!!!!!!!!WB-37*E!!!!!!!!!XR#2%BC!!!!!!!!!Z"#2&amp;.&amp;!!!!!!!!![273624!!!!!!!!!\B%6%B1!!!!!!!!!]R.65F%!!!!!!!!!_")36.5!!!!!!!!!`271V21!!!!!!!!"!B'6%&amp;#!!!!!!!!""Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"A!!!!!!!!!!0````]!!!!!!!!!P!!!!!!!!!!!`````Q!!!!!!!!$1!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!""!!!!!!!!!!!`````Q!!!!!!!!%-!!!!!!!!!!,`````!!!!!!!!!41!!!!!!!!!!0````]!!!!!!!!"4!!!!!!!!!!!`````Q!!!!!!!!'=!!!!!!!!!!$`````!!!!!!!!!;Q!!!!!!!!!!@````]!!!!!!!!$2!!!!!!!!!!#`````Q!!!!!!!!29!!!!!!!!!!4`````!!!!!!!!"2!!!!!!!!!!"`````]!!!!!!!!&amp;)!!!!!!!!!!)`````Q!!!!!!!!5Q!!!!!!!!!!H`````!!!!!!!!"5!!!!!!!!!!#P````]!!!!!!!!&amp;5!!!!!!!!!!!`````Q!!!!!!!!6A!!!!!!!!!!$`````!!!!!!!!"8A!!!!!!!!!!0````]!!!!!!!!&amp;D!!!!!!!!!!!`````Q!!!!!!!!91!!!!!!!!!!$`````!!!!!!!!#B1!!!!!!!!!!0````]!!!!!!!!+(!!!!!!!!!!!`````Q!!!!!!!!IM!!!!!!!!!!$`````!!!!!!!!$[A!!!!!!!!!!0````]!!!!!!!!0M!!!!!!!!!!!`````Q!!!!!!!!_Y!!!!!!!!!!$`````!!!!!!!!$]A!!!!!!!!!!0````]!!!!!!!!1-!!!!!!!!!!!`````Q!!!!!!!"!Y!!!!!!!!!!$`````!!!!!!!!%X!!!!!!!!!!!0````]!!!!!!!!4?!!!!!!!!!!!`````Q!!!!!!!"/!!!!!!!!!!!$`````!!!!!!!!%[Q!!!!!!!!!A0````]!!!!!!!!5X!!!!!!*5G6M98EO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!1V3:7RB?3ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!%!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!5!&amp;%!B$V.X;82D;'FO:V.U982V=Q!,1!I!"5.P&gt;7ZU!!^!#A!*1WBB&lt;GZF&lt;%ZP!!^!#A!)5X&gt;J&gt;'.I4G]!!&amp;)!]&gt;*,OBM!!!!#$6*F&lt;'&amp;Z,GRW9WRB=X-*5G6M98EO9X2M!$*!5!!%!!!!!1!#!!-?1WRV=X2F=C"B&gt;8-A3WRB=X.F&lt;H"S;8:B&gt;'2B&gt;'6O!!!"!!1!!!!%`````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!!!!!1!!!!!!!A!!!!!&amp;!"2!)1^4&gt;WFU9WBJ&lt;G&gt;4&gt;'&amp;U&gt;8-!#U!(!!6$&lt;X6O&gt;!!01!=!#5.I97ZO:7R/&lt;Q!01!=!#&amp;.X;82D;%ZP!!"3!0(33_1N!!!!!AV3:7RB?3ZM&gt;G.M98.T#6*F&lt;'&amp;Z,G.U&lt;!!S1&amp;!!"!!!!!%!!A!$(E.M&gt;8.U:8)A986T)%NM98.T:7ZQ=GFW982E982F&lt;A!!!1!%!!!!"!!!!!!!!!!"!!!!!A!!!!-!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!-!!!!!"A!51#%05X&gt;J&gt;'.I;7ZH5X2B&gt;(6T!!N!"Q!&amp;1W^V&lt;H1!$U!(!!F$;'&amp;O&lt;G6M4G]!$U!(!!B4&gt;WFU9WB/&lt;Q!!$E!Q`````Q2/97VF!!"5!0(33_6D!!!!!AV3:7RB?3ZM&gt;G.M98.T#6*F&lt;'&amp;Z,G.U&lt;!!U1&amp;!!"1!!!!%!!A!$!!1?1WRV=X2F=C"B&gt;8-A3WRB=X.F&lt;H"S;8:B&gt;'2B&gt;'6O!!!"!!5!!!!&amp;!!!!!!!!!!%!!!!#!!!!!`````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.3</Property>
	<Item Name="Relay.ctl" Type="Class Private Data" URL="Relay.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="DataOperation" Type="Folder">
		<Item Name="ChannelNo" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ChannelNo</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ChannelNo</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readChannelNo.vi" Type="VI" URL="../readChannelNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"Q!*1WBB&lt;GZF&lt;%ZP!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B&amp;;7ZH97ZH+1"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeChannelNo.vi" Type="VI" URL="../writeChannelNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"Q!*1WBB&lt;GZF&lt;%ZP!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B&amp;;7ZH97ZH+1"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Count" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Count</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Count</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readCount.vi" Type="VI" URL="../readCount.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"Q!&amp;1W^V&lt;H1!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%&amp;V=W&gt;B&lt;G=J!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%6J&lt;G&gt;B&lt;G=J!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeCount.vi" Type="VI" URL="../writeCount.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"Q!&amp;1W^V&lt;H1!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%6J&lt;G&gt;B&lt;G=J!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Name" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Name</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Name</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readName.vi" Type="VI" URL="../readName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]%4G&amp;N:1!!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%&amp;V=W&gt;B&lt;G=J!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%6J&lt;G&gt;B&lt;G=J!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeName.vi" Type="VI" URL="../writeName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!-0````]%4G&amp;N:1!!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%6J&lt;G&gt;B&lt;G=J!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!B!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="SwitchingStatus" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SwitchingStatus</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SwitchingStatus</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readSwitchingStatus.vi" Type="VI" URL="../readSwitchingStatus.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!)1^4&gt;WFU9WBJ&lt;G&gt;4&gt;'&amp;U&gt;8-!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%&amp;V=W&gt;B&lt;G=J!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%6J&lt;G&gt;B&lt;G=J!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeSwitchingStatus.vi" Type="VI" URL="../writeSwitchingStatus.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1^4&gt;WFU9WBJ&lt;G&gt;4&gt;'&amp;U&gt;8-!+%"Q!"Y!!!].5G6M98EO&lt;(:D&lt;'&amp;T=Q!05G6M98EA+%6J&lt;G&gt;B&lt;G=J!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="SwitchNo" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SwitchNo</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SwitchNo</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readSwitchNo.vi" Type="VI" URL="../readSwitchNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"Q!)5X&gt;J&gt;'.I4G]!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B&amp;;7ZH97ZH+1"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeSwitchNo.vi" Type="VI" URL="../writeSwitchNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B"&gt;8.H97ZH+1!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"Q!)5X&gt;J&gt;'.I4G]!!#B!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!$V*F&lt;'&amp;Z)#B&amp;;7ZH97ZH+1"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Constructor.vi" Type="VI" URL="../Constructor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%+!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=E^V&gt;!!!"!!!!#*!=!!?!!!0$6*F&lt;'&amp;Z,GRW9WRB=X-!#&amp;*F&lt;'&amp;Z4X6U!!!/1$$`````"%ZB&lt;75!!"2!5!!$!!!!!1!#"U6S=G^S37Y!#U!(!!6$&lt;X6O&gt;!!01!=!#5.I97ZO:7R/&lt;Q!01!=!#&amp;.X;82D;%ZP!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!9!"Q!)!!E!#A)!!(A!!!U)!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!))!!!!#!!!!!A!!!!)!!!!#!!!!!!"!!M!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
</LVClass>
