RSRC
 LVCCLBVW  ,  T         � �0           < � @       ����            Ťf��ZN����U�@j          x�,dM�EA��ڗ�~���ُ ��	���B~       �.F4EOM�7���M��   �ʞ:�6��]y�nbO   2�*�� ��Ò�`8�m�   0_,+�40�����G>   & LVCCFPGA_I2C IO Cluster.ctl                     x�c�d```4``b f  	� �    X  3x�c`��� H1200} �,h�`Ʀ6@6����33@�
bv���8�8BD�Z���	jD��
�@7�]  *�*�   & VIDSFPGA_I2C IO Cluster.ctl           I  x�K`d`�4�0[ �����SR��43����� �P���a0������EE��������U� ��t��tv���t��j>�s�� �d`x� ��T��P!���*8�� 5��E�X��6�͓�n
k~# Ԩ�]�"��ft2�2��> ��h!��q�R ������o�9��� T3Xc�A�L�r�ۉ g�|Ɂ�R �0#�}@�	t�@<��v�A� �,�0���\��w���#q��~�g|���r��&#� - ��!t�V� �� �����P�X��������@4p�wqE�� ��ig       �'  12.0     �  12.0     �'  12.0     �  12.0     �'  12.0                       8   4Specifies the FPGA IO items for the I2C port to use.   ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������         & FPHPFPGA_I2C IO Cluster.ctl           � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       ]  Jx��V]OU~��l9�,2�|mC]Xg�j��Fm%ila�+�� �Ek�l�M�5�[��b�AS/��¤I����o�Y�ɨibӤɦ�Մ`"��{���lMC����d8���<����Z�\6�H��8i@P�	@!A��H��o �b�:*�J�̀��w��,<v��z���u��HA�U�%<(%��6y�����*)	���5zU�b6к�5� % J�(j��s�5��$h�� )z]N.��1�O<����	�]PX�� ���q�ax��U0�����8&�1�熓G���a�3P%�YE�����'�5�n���({i�>T�ڏ@��R��5,
��!��ۀD�{���uʀ5]xD������+N��а��0�i(��<t=�E,�j�&���s��H(z�듅�d�"��I�\�0??���Յ�Gh�\,���"|X���Έeug�2�(���A�k�<�V�G��@���#�L�L�Re}d�����UT��
�B��Iɽf�<2M"j2U0�se�L��g��SM��7�4��*�tnn�R���)��*b�U�e���L���_`2\�Ǎ"5�.������u�4�N!d�L	���C� L�j,��V�0���F�=������VY���ր$J�\<e��S�4�j �����%�`����@����)Uo`�Z�1���J9�5�|��ibp�Z�h�)jJfw	y)h�0�<cȗ�لXH�!{��|텨�f�h�l�u�-R�.7OcV�[ne�7n-���U�&�<��Xbu��m�:������N���g�+��=?�����}d,�{�b�U��,�^���X@���|2�����.ok7=��z��=[|�3cq���������������h.=���&ҁ�ܹ�������p.{邚�M/�����LO]���T��qoCX�D��h�#�(\��G�4�[>ϙQ��WF�bʱ�c��r�9���N��1���S";�����w���:��m�r�?p����N9ep��C�W"���/�UiJV̕q�[�����^"�c�+4)}K�	vʢ'h�tx�h���j�N��[8YFj��-����� 8
!|         <   & BDHPFPGA_I2C IO Cluster.ctl            e   ux�c``(�`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-���W)b��z�\�8Se�<� e�                    h   (                                        �                    �     @UUA N 	                                                   ��*>  �>��*>  �>     @   ?          |  �x��U]��@�KAXVw�}��3�����6Ӹ.��QK;����ajd���-��&z��k��ov��~�s��3� ���_�@��, lA���1چ/(wL�&�T#R�5����T�ݣB/B�K|sD_w�iG�0U���@�9x5.Y���Oy��M}�K�7��OU��3f��{E�s��1�0���!g�{���T!�$���i�5U/M/��V�V�VN�x2�f���q�dH�+Z��:�}��0U��R��`Ԁ����rj"]"��Ie�KE��5��tL�>c��FfRd���n8c܆�1i��\w��@��P��]�����IL�娕�|��5u�Q]�9���O�$lZzi�5CFS%�,F�����\����7����+��O�����F����n=�ѹ~����7Ë4�꼮��kc�/Ğڎ��yn�-J��No��6�u�S��}v�.����I�������:�A^&/���E�����@�.<�;�s���]���<��
۵<(�7�Bm�����9PB�;ȿ��U��\���{��j�v��Mf�W����q�Yh!�@�������\�Ef?Z��[p��Ս�*�(9���}8�#��#���o�/�V   e       H       � �   Q       � �   Z       � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  ,  T                     4  <   LVSR      `RTSG      tOBSG      �CCSG      �LIvi      �CONP      �TM80      �DFDS      �LIds       VICD      vers     (GCPR      �STRG      �ICON      �icl8      �CPC2      �LIfp      �STR      FPHb      ,FPSE      @LIbd      TBDHb      hBDSE      |DTHP      �MUID      �HIST      �PRT       �VCTP      �FTAB      �                        ����       �        ����       �        ����       �        ����       �        ����       �        ����       �        ����               ����      |        ����      �       ����      �       ����             ����             	����      (       
����      8        ����      H        ����      \        ����      �        ����              ����               ����      (       ����      T       ����      T        ����      T        ����      �        ����      �        ����      �        ����      X        ����      `        ����      h        ����      p        ����      �        ����              �����      �    FPGA_I2C IO Cluster.ctl