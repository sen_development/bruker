﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&gt;D5F.31QU+!!.-6E.$4%*76Q!!%R!!!!24!!!!)!!!%P!!!!!4!!!!!1Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!!!#1&amp;1#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!!$3$-2\@3R,F]5UNRM*H&gt;)!!!!-!!!!%!!$!!!=$#D/$,O42,#.P;NB":QPV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!LPY&lt;UXNWFUK8(LNK9&amp;*+$A%!!!$`````!!!!%,Z'*:%"D?B&gt;IRZW&amp;^BG-EI!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!G!!!!*HC=9_"E9'JAO-!!R)R!T.4!^!0)`A$C-QBQ1'59!-@U#P%!!!!!!%A!!!%9?*RD9-!%`Y%!3$%S-$"&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=6!X5$URHA0A%ODH-7-Q'!(`J+#)!!!!-!!&amp;73524!!!!!!!$!!!"EA!!!O"YH*P!S-#1;7RB^A")-Q/R+%-$1X*_3CI8!Z$0!!%HG"B)"A&amp;1`6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6T,Q!I'E=$28+$#5MBA?C$B^P-''%7!QT-AL&gt;@A'A$JHO%+$&amp;44*!&amp;Q/&gt;I.$:J1"E12U#N)A(;C(99L!JL1@YJRTAXX:C"]BU@N?$T5=%A%ZQ!:H(!42"Y/"$FOZ'$;"=\U11#24C[1TBE$DOQK%D"O1THOA%/L_4"_:,$LD\QU!'F+A)&gt;*K!,':BB&amp;H9T8&lt;=110M$!=2#*5"I3IA6!')WA(WU2'/O-0Q]&amp;\\_NYO5$AB2[%"%)05[T%Q-D##Z2A:;K&amp;S.E!W%V1-&amp;H=ANA!U')71^-"MU5!3AQ7X!&gt;2/E-Q&lt;K$I1_R/5$&lt;+@$3JG#^1T!=JW!&lt;)4I'RP)0M"F"U%:!N!W:&amp;!NA)DB"U(:4P\O\ACJT.9GA5!(^##3!!!!!!!$B5"A!=!!Q9R.3YQ,D%!!!!!!!!-&amp;1#!!!!$"$%V,D!!!!!!$B5"A!=!!Q9R.3YQ,D%!!!!!!!!-&amp;1#!!!!$"$%V,D!!!!!!$B5"A!=!!Q9R.3YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!#ZO1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!#ZU=8,U&lt;E!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!#ZU=7`P\_`S^'Z!!!!!!!!!!!!!!!!!!!!!0``!!#ZU=7`P\_`P\_`P]P2O1!!!!!!!!!!!!!!!!!!``]!S]7`P\_`P\_`P\_`P\`,U1!!!!!!!!!!!!!!!!$``Q$&amp;R&lt;_`P\_`P\_`P\_`P``,!!!!!!!!!!!!!!!!!0``!-8,S]7`P\_`P\_`P````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P&amp;P\_`P```````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,R&gt;(````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$,S]P,S]P,S````````]P,!!!!!!!!!!!!!!!!!0``!!$&amp;R=P,S]P,`````]P2R1!!!!!!!!!!!!!!!!!!``]!!!!!R=P,S]P``]P,R1!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!-8,S]P,PQ!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!$&amp;PQ!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!!A!"!!!!!!!-!!&amp;'5%B1!!!!!!!$!!!!=R5!A!!!!!!"!!1!!!!"!!!!!!!#!!!!$72J=X"M98F';7RU:8)6!)!!!!!!!1!&amp;!!=!!!%!!!!!!!%!!!!!!!!!#82Z='6$&lt;'&amp;T=R5!A!!!!!!"!!A!-0````]!!1!!!!!!$'ZJ&lt;'.J5W6T=WFP&lt;A!!!!!!!!!%7Q!!#.FYH,6787B&lt;:2B_P_2E_Z+F\5HN4T+G/3UH&gt;:VWDI&amp;WBBJ(&gt;DJ7NX7&lt;7:XWIFX7=T4",#HZ%&lt;XJP$A5KOR+VYP"1&amp;AJ#))8:8A\2TK&amp;A\I&lt;&amp;4=BLJ@?[)23U:U=X_`]Z'^&gt;^-95$B`J_TT0_\X0=VY#1,`HA[YK,/B!_%U]H.$"*WM%I$R#Q@Y-,1)`2@Y'UB-C/BSG5`Q^6Z8MU=%P;U0UA,1%PW/V]9NR&amp;;Y1,X]@3X@Q)34T[&gt;!F;`W"6]1+,X[_2VTS/+Q"/-V@*F88[W*YCVZ7=SA)[F\W$)S1+B"JA/0+AZV(F7SBK/1P+(EF0:&gt;326&lt;A(;%BE^WL!S^J(8GR]D339R?X4(9S!SMEZ\!$MO_&amp;N&lt;7V/CBAA9&lt;-DAYRT!L3XC!T&lt;4$&gt;EP:E1;Q];W*]*A:V&lt;DA[5LD[%&lt;M'!\6#HZ!U(K')?^?Z`9KD*98VQS:O9W-$=@CU=3E&gt;?M2+H)&lt;I@@EO[4CD@A%%3(G7'D];HT*]9*):9N:WIS?2+/%4?%\IM%`68/?"=\4C=.VUB(-='5.(S&amp;(4%;_UR%0&gt;EOP`9IG%FDRV*&amp;.CFAD*5E%YHEE7#EJW0J^_*VG5EU5F_[B&amp;,UG;&gt;Z2&gt;H[G:19&amp;?Y#!'G=:RZW"V&gt;25HA-][.)&lt;1@L&amp;3QQ8-[]T82C\82]Z5[[.\'5=H`8HW,4;_3.46H.`R?HY09HY$@'N&lt;4@F^`H`.\QM9KY77`%)+BO'^.FE=N5!._2V'6"23&lt;4#(%(/J+&lt;`$K".V&gt;.LF^]6(]TPM;.8SO\S]X)2$%[+V`(+%70F6\RA0D9=MR8]:P])I,$;E?!K&gt;96PE._10)H^4:G8GQK29?&lt;5JMQ2N7'BPA_!#+\?^%_B%PH22S2;&amp;9]GMH&amp;'%5[6CCS/IJ'T\&gt;D#F]`^2K?/V_+RU]O3MJ&gt;+C],%/;6SPN%?KB-]YQ?OS2]KOP.-MW[8$WRD_AW,FF08?G.(Q1!,[1,9'(1BDN9M0W@:^S!4M]Q=UZ!X&lt;AF&gt;US-C;Q+BW.&lt;S#(OB%KI4DIB%Q841-!]HR;&gt;V*(9$X/5Y6W4@?%@'GT_&lt;U[X!2&gt;SB30G..S=]IX5C['`TW5'L&gt;O96SW%J.0_R5"8:1"VA*0WCX7[+$6P0&lt;FD,RZN,;X4#=75HTY(9B6B]X72]=`P6"B_07ZO.SH7P:,OSTOR\MRW_8?1RWZ!W)4!0OG$`M\^'RAK3&amp;-02&gt;6C_5=?\!8LL"BTQ`X@U:'SAN@G*/BH/]WPK[\NP76QXHWQXH&gt;?P/&lt;*?ZBR&lt;&gt;E:FT%?+*C*\)/&gt;@[.`\4:Y]&gt;Q0YY$#GM@^O:47@GUK7#EE`/J`@PKX&gt;&gt;&lt;/T[!70DJ`%`8_JQ)2"E&lt;W%Z&amp;"O,R\:^4=;?C]?%&gt;%&amp;)#HHF4&gt;RFW4F&amp;+/;%9EI2UL8[`&gt;YAH9Z%/&gt;SWW/^EQT+C^W[8;D]G;%+5D(9&amp;`$CW&gt;5G(8OF)^X@]",&lt;GJR.UH,_'/RP8&gt;R_^2D@,0^2_&lt;J20V*$K(8DA`ES]'K&lt;"@Q"&lt;'I23!!!!!!1!!!!Q!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!X)!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!+Q6!)!!!!!!!1!)!$$`````!!%!!!!!!*!!!!!$!#I!.`````]!"25!A!!!!!!"!!1!!!!"!!!!!!!!#GRD;6.F=X.J&lt;WY!!%:!=!!6$'ZJ&lt;'.J5W6T=WFP&lt;A!!!1!!#GRD;6.F=X.J&lt;WY!&amp;1#!!!!!!!%!"!!!!!%!!!!!!!!.6E*@2%V.8UBB&lt;G2M:1!91&amp;!!!1!"$F:#8U2.43ZM&gt;G.M98.T!!!"!!)!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+25!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E6!)!!!!!!!1!&amp;!!=!!!%!!.*Y=-=!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'25!A!!!!!!"!!5!"Q!!!1!!UHBQRQ!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!#M&amp;1#!!!!!!!%!#!!Q`````Q!"!!!!!!#1!!!!!Q!K!$@`````!!56!)!!!!!!!1!%!!!!!1!!!!!!!!JM9WF4:8.T;7^O!!"'1(!!&amp;1RO;7RD;6.F=X.J&lt;WY!!!%!!!JM9WF4:8.T;7^O!"5!A!!!!!!"!!1!!!!"!!!!!!!!$6:#8U2.46^)97ZE&lt;'5!'%"1!!%!!1Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;1#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!*Q6!)!!!!!!!Q!K!$@`````!!56!)!!!!!!!1!%!!!!!1!!!!!!!!JM9WF4:8.T;7^O!!"'1(!!&amp;1RO;7RD;6.F=X.J&lt;WY!!!%!!!JM9WF4:8.T;7^O!"5!A!!!!!!"!!1!!!!"!!!!!!!!$6:#8U2.46^)97ZE&lt;'5!'%"1!!%!!1Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!!!!!!!!!!1!"!!,!!!!"!!!!(1!!!!I!!!!!A!!"!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2]!!!)4?*S&gt;5-N+QU!50=GE.IVJL9`K4L)5%4=O[D+I&amp;%%K2=&amp;N$8F)9%B#-SF&gt;^O`=_S8["8IS#;DI1LS(8/ZLTLEX!!ZQD0%\$:U2VK!:M,48ZMAQP9`,-MUT9/)8',F:_L6G@*`ZD;0`=$'`GE\HVU%7S2CX?(N:&amp;=^MG)/G=SK8I1T+UGH45%G=_$/_.QYP:67K?/%&amp;6?H&gt;V%.R6CT3:;#C1-85\R"Q%&amp;(N&amp;9+*;&amp;/=-?HZ89B%0M(WVS+`5UUBTB--S3`1B9W?K+)%Z`D8&lt;8$VHJN#L3,KTG$3OTD[]`:G\4:K2U9&lt;D[3U-#'2BSU-O7+.&lt;8VG!^(C:_7TM[02`!9$OYR#T&gt;OM0K#I2=U_ITV?:@$&lt;:TSIJT]!F&gt;J8X1!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!4%!!!"&amp;-!!!!A!!!3]!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)(:F=H-!!!!%!!!#.&amp;.$5V)!!!!!!!!#G%&gt;$5&amp;)!!!!!!!!#L%F$4UY!!!!!!!!#Q'FD&lt;$A!!!!!!!!#V%.11T)!!!!!!!!#[%R*:H!!!!!!!!!#`&amp;.55C!!!!!!!!!$%%:13')!!!!!!!!$*%:15U5!!!!!!!!$/&amp;:12&amp;!!!!!!!!!$4%R*9G1!!!!!!!!$9%*%3')!!!!!!!!$&gt;%*%5U5!!!!!!!!$C&amp;:*6&amp;-!!!!!!!!$H%253&amp;!!!!!!!!!$M%V6351!!!!!!!!$R%B*5V1!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'!!!!!!!!!!!`````Q!!!!!!!!#M!!!!!!!!!!$`````!!!!!!!!!-!!!!!!!!!!!0````]!!!!!!!!!S!!!!!!!!!!!`````Q!!!!!!!!$U!!!!!!!!!!$`````!!!!!!!!!0Q!!!!!!!!!!0````]!!!!!!!!"+!!!!!!!!!!!`````Q!!!!!!!!&amp;U!!!!!!!!!!$`````!!!!!!!!!91!!!!!!!!!"0````]!!!!!!!!$(!!!!!!!!!!(`````Q!!!!!!!!-Q!!!!!!!!!!D`````!!!!!!!!!U!!!!!!!!!!#@````]!!!!!!!!$6!!!!!!!!!!+`````Q!!!!!!!!.E!!!!!!!!!!$`````!!!!!!!!!XA!!!!!!!!!!0````]!!!!!!!!$E!!!!!!!!!!!`````Q!!!!!!!!/E!!!!!!!!!!$`````!!!!!!!!"#A!!!!!!!!!!0````]!!!!!!!!),!!!!!!!!!!!`````Q!!!!!!!!AU!!!!!!!!!!,`````!!!!!!!!#%1!!!!!!!!!!0````]!!!!!!!!)P!!!!!!!!!!!`````Q!!!!!!!!U=!!!!!!!!!!$`````!!!!!!!!$31!!!!!!!!!!0````]!!!!!!!!.,!!!!!!!!!!!`````Q!!!!!!!!U]!!!!!!!!!!$`````!!!!!!!!$;1!!!!!!!!!!0````]!!!!!!!!.L!!!!!!!!!!!`````Q!!!!!!!"%E!!!!!!!!!!$`````!!!!!!!!%3Q!!!!!!!!!!0````]!!!!!!!!2.!!!!!!!!!!!`````Q!!!!!!!"&amp;A!!!!!!!!!)$`````!!!!!!!!%I1!!!!!#F:#8U2.43ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!1Z71F^%45UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!$!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!)!.E!X`````Q!&amp;&amp;1#!!!!!!!%!"!!!!!%!!!!!!!!*&lt;'.J2'6W;7.F$6:#8U2.46^)97ZE&lt;'5!4A$RUHBO8A!!!!)/6E*@2%V.,GRW9WRB=X-+6E*@2%V.,G.U&lt;!!M1&amp;!!!1!!(E.M&gt;8.U:8)A986T)%NM98.T:7ZQ=GFW982E982F&lt;A!!!1!"!!!!!@````]!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!)!!!!!!Q!K!$@`````!!56!)!!!!!!!1!%!!!!!1!!!!!!!!JM9WF4:8.T;7^O!!"'1(!!&amp;1RO;7RD;6.F=X.J&lt;WY!!!%!!!JM9WF4:8.T;7^O!"5!A!!!!!!"!!1!!!!"!!!!!!!!$6:#8U2.46^)97ZE&lt;'5!4A$RUHBQRQ!!!!)/6E*@2%V.,GRW9WRB=X-+6E*@2%V.,G.U&lt;!!M1&amp;!!!1!"(E.M&gt;8.U:8)A986T)%NM98.T:7ZQ=GFW982E982F&lt;A!!!1!#!!!!!@````]!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Item Name="VB_DMM.ctl" Type="Class Private Data" URL="VB_DMM.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="VB_DMM_Handle" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">VB_DMM_Handle</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VB_DMM_Handle</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="readVBDMMHandle.vi" Type="VI" URL="../readVBDMMHandle.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&amp;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#I!.`````]!"25!A!!!!!!"!!1!!!!"!!!!!!!!#GRD;6.F=X.J&lt;WY!!%:!=!!6$'ZJ&lt;'.J5W6T=WFP&lt;A!!!1!&amp;#GRD;6.F=X.J&lt;WY!&amp;1#!!!!!!!%!"!!!!!%!!!!!!!!.6E*@2%V.8UBB&lt;G2M:1!M1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!%&amp;:#8U2.43!I186T:W&amp;O:SE!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,%"Q!"Y!!"!/6E*@2%V.,GRW9WRB=X-!!""71F^%45UA+%6J&lt;G&gt;B&lt;G=J!!"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="writeVBDMMHandle.vi" Type="VI" URL="../writeVBDMMHandle.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'&amp;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!16E*@2%V.)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K!$@`````!!56!)!!!!!!!1!%!!!!!1!!!!!!!!JM9WF4:8.T;7^O!!"'1(!!&amp;1RO;7RD;6.F=X.J&lt;WY!!!%!"QJM9WF4:8.T;7^O!"5!A!!!!!!"!!1!!!!"!!!!!!!!$6:#8U2.46^)97ZE&lt;'5!,%"Q!"Y!!"!/6E*@2%V.,GRW9WRB=X-!!""71F^%45UA+%6J&lt;G&gt;B&lt;G=J!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="ConfigureACCurrent.vi" Type="VI" URL="../ConfigureACCurrent.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'5!$!"."&gt;82P)&amp;*B&lt;G&gt;F)&amp;2F=GVJ&lt;G&amp;M!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%6J&lt;G&gt;B&lt;G=!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!")!!!#3!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="ConfigureDCCurrent.vi" Type="VI" URL="../ConfigureDCCurrent.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'5!$!"."&gt;82P)&amp;*B&lt;G&gt;F)&amp;2F=GVJ&lt;G&amp;M!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%6J&lt;G&gt;B&lt;G=!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!")!!!#3!!!!!!%!#1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="ConfigureDCVoltage.vi" Type="VI" URL="../ConfigureDCVoltage.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;U!$!""*&lt;H"V&gt;#"3:8.J=X2B&lt;G.F!!!K1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!$F:#8U2.43"&amp;;7ZH97ZH!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Destructor.vi" Type="VI" URL="../Destructor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%2!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"!/6E*@2%V.,GRW9WRB=X-!!!Z71F^%45UA27FO:W&amp;O:Q!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Export.vi" Type="VI" URL="../Export.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!)%!S`````R&gt;$&lt;WZG;7&gt;V=G&amp;U;7^O)%:J&lt;'5A4G&amp;N:1!K1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!$F:#8U2.43"&amp;;7ZH97ZH!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Import.vi" Type="VI" URL="../Import.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!)%!S`````R&gt;$&lt;WZG;7&gt;V=G&amp;U;7^O)%:J&lt;'5A4G&amp;N:1!K1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!$F:#8U2.43"&amp;;7ZH97ZH!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!3!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="initializeVB_DMM.vi" Type="VI" URL="../initializeVB_DMM.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=E^V&gt;!!!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!"2!5!!$!!!!!1!#"U6S=G^S37Y!#E!B"6*F=W6U!$2!.`````]!"25!A!!!!!!"!!1!!!!"!!!!!!!!#7RD;52F&gt;GFD:1J%:8:J9W6/97VF!!!K1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!$F:#8U2.43"&amp;;7ZH97ZH!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!I!!!%3!!!!EA!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Read.vi" Type="VI" URL="../Read.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%C!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!,476B=X6S:7VF&lt;H1!+E"Q!"Y!!"!/6E*@2%V.,GRW9WRB=X-!!!Z71F^%45UA186T:W&amp;O:Q!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!$F:#8U2.43"&amp;;7ZH97ZH!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="Reset.vi" Type="VI" URL="../Reset.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%2!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"!/6E*@2%V.,GRW9WRB=X-!!!Z71F^%45UA27FO:W&amp;O:Q!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="SetMode.vi" Type="VI" URL="../SetMode.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;$!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!1$F:#8U2.43ZM&gt;G.M98.T!!!/6E*@2%V.)%&amp;V=W&gt;B&lt;G=!!".!#A!-47&amp;O&gt;7&amp;M)&amp;*B&lt;G&gt;F!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!)1J"&gt;82P)&amp;*B&lt;G&gt;F!!!01!-!#%:V&lt;G.U;7^O!!!K1(!!(A!!%!Z71F^%45UO&lt;(:D&lt;'&amp;T=Q!!$F:#8U2.43"&amp;;7ZH97ZH!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!'!!1!"Q!)!!E!#A-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!I!!!!!!!!!#A!!!")!!!!3!!!!EA!!!!!"!!M!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
</LVClass>
