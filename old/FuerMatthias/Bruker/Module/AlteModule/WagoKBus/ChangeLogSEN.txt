17.08.2016
MK
Aus dem VI WriteDigitalOutput.vi sind 2 neue VI erstellt worden.
	- WriteDigitalOutputBit.vi f�r den Zugriff auf einzelne Pins
	- WriteDigitalOutputByte.vi f�r den Zugriff auf ein komplettes Byte (Die Maske ist permanent auf 254 gesetzt, damit werden alle 8 Bit geschrieben.
	! Der KBusSequence.seq wurden beide Typen als Sequences hinzugef�gt
	WriteDigitalOutput muss wieder hinzugef�gt werden!! (Kompatibilit�t zu alten UFT Programmen)


