﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.4</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"N`5F.31QU+!!.-6E.$4%*76Q!!&amp;R!!!!2P!!!!)!!!&amp;P!!!!!4!!!!!1Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;A#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!*6-R$4B:16!B=$+6RP`H!9!!!!-!!!!%!!!!!!.:E^'`1$K1LXSS9+1NYI'V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!%[D*=;&lt;ZXEKX?8@!TX_9!A%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$1'F&gt;$Q5HT,@Z6(PXJ&lt;@KJ!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)1!!!"BYH'.A9W"K9,D!!-3-$EQ.4"F!VA?'!!9!0[%&amp;FA!!!!!!!")!!!!-?*RD9'(AA%)'!!$Y!#5!!!!!!%E!!!%9?*RD9-!%`Y%!3$%S-$!^!^)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!V`"!JJON!@!,&gt;(+AEQQ5E-1#DFSEJ!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!:9!!!-]?*R&lt;Q-D!E'FM9@9"3$-$M2B$!U.S@EIK,Q/1TQ!"&lt;ZA93!9"50V;;/+'"Q[H!9%?PXQ,G.`]BK@&lt;257AO5:&amp;AKF5J.N(2;442Y7FEU8FR:````]X(_%ZX/W2=^T2"K3WGQ-I@NR&amp;B10%!&gt;)M)0J`9!:)&amp;;JZ-JV!73!NA;1"&lt;C#+`1&amp;!62Q.&amp;=I-*3S'"[)/(W]Q991Y&amp;/;%+'TO*&gt;\]ZD==1%]*((T)UNWI!?4X4A321#'?TB!/C?-O($JC1$\D#:#"H4QQ8X0!`2-'-K"%2;$4"'12#S0-IG[WYQY;Y("Q%)&amp;1'2#K!E)6A+A&gt;9"==Y9A\$!``N;`P\7)&amp;UMB2;A$%)06[$)Q-D'!Z2I;V5$E&lt;)*M*+A;,3R"&lt;!2J-'EB[8+"CD!TW=,&amp;KO"[%W'UE?ZH![BE:`D$!^!,NAZI$=B-&lt;6-Q8+(9!SAY"MC&gt;!W&gt;&amp;!^A=I/QH)&amp;I#S-Y&amp;M!U9)/Q`+&gt;P:X=56/C["U$5PDI%"-,EAOU[NWUEE.NT'RUQE'Q6J2E(BOA9%"OA21(!!JO*?&lt;!!!!!!$S!!!"&lt;(C=-W"A9-AUND$\!+3:'2E9R"A;'*,T5V):E)!.)Y)&gt;("\7`%;AWU6&amp;I&gt;.&amp;B;@&lt;2U7FUU?&amp;IYOBAR()ZOFV:!1,O[BQ^!9Q&gt;I;I=+2V?T+G\7*G3'P_S:,'\X1RL4/!%3D.UVP"V&amp;E$6&amp;GCQN&amp;]D+E43(5\A#C74B;6&amp;X`_```@?K$5O4??J4.'B;@Z'!N9A4M,GA+F8G=7E(UQ"=ZI#PB&gt;$D1@%9C,DTY-&gt;``;V`&gt;WA&lt;S$Z#5'!S$/!IIQ!WEG)):Z(M2O1W)P2V)0!M\_,K\IY11S6Q3)EQO3S`3KH823QWV-\(3#1&lt;!7!)[Y6?5!!!!!!3=!!!'A?*QT9'"AS$3W-&amp;.A:'"A"G)RBA;'Z0S56!9EE-/)9)='BY=VPR(I&gt;F&amp;2[822Y?HW5&gt;(I^&amp;(B;,"EY*^[K-%#3"\OO'&gt;QT?"S#4@`NG0.(RB@LA(K[AVA\!R2Y?GN:/SM5?&amp;)[X:A4,.W:/3@=I#`N1%E8=4571+5DG@KD&amp;(B[(:C[P:H!JL+UMGC]O,0````GX_!V*&lt;'^N;R!08T^';T&gt;/9!V&lt;GR&gt;(OT9&amp;&amp;HV?P/!H*=LSO)YOBW:_HWR62XE(`&lt;38[8%ZUH8P0S&lt;TPQGA.%-!/*ZC-#=@(2M9=B`FX\_NYOE0?2AI$"!)C6'#19G)%U%R!81M6"\*F)\).)[E(!W&gt;`&amp;&amp;4V=1?;+!H&amp;S&lt;I'"A6[VEUZKO)W*H5YQ#.9#R1&amp;"&lt;GP.!!!!!!Q7!)!:!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"E!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!'1!!"$%W,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!"15!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!";V:A[U&amp;!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!";V:,S]P,Y/N"1!!!!!!!!!!!!!!!!!!!!$``Q!!";V:,S]P,S]P,S_$L15!!!!!!!!!!!!!!!!!!0``!).:,S]P,S]P,S]P,S]PA[U!!!!!!!!!!!!!!!!!``]!76EP,S]P,S]P,S]P,S`_AQ!!!!!!!!!!!!!!!!$``Q":AY.:,S]P,S]P,S`_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$73]P,S`_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AVGN`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!AY/$AY/$AY0_`P\_`P[$AQ!!!!!!!!!!!!!!!!$``Q!!76G$AY/$A`\_`P[$L6E!!!!!!!!!!!!!!!!!!0``!!!!!&amp;G$AY/$`P[$AVE!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!":AY/$AS]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!73]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!!$!!"2F")5!!!!!!!!Q!!"@5!!")D?*S^G&amp;^M5X55R]`PLBWX;]&gt;O"RPL!L3&lt;NW5#GTDFDQ36S#VEMD`]&gt;`CAF0&lt;+;OI[VIW)*O"$H=Q!U7"1U"LD![]_,-18(AQJ;.)9S2Z-"'0:(HAB2FY)/LGLZ`?\P&lt;WX^Z:WQA#38W[;]TXH&gt;]\Z`-\PXA(5?Y5G&lt;B;/+5#%O`D1L5".*%-!UOU]Z0]&amp;RE$93`Y&amp;MNB$&amp;.D-\R6O=,.EK1+O3#&lt;!LZ('Y1Z;ZW\GPC88O(0#&amp;*J7#RZU6K.!833TR0WKG"8%\Z;+YX&lt;.[S,I&amp;U[27;Z@^.\D4S8D'"#3&lt;82VNZ.:)&amp;+,T::O8&lt;B6(ES-S--(Z7%Z'BZ)CN4!U=Z\G(?(!I+5K2U7MSP1/?\C-P0/X3*:\J\G(&gt;"\'UR-4/ACNSI+M"VN1!X*!J!\X+USGHIJMSQB:F=T41X49*Q\7BT*/XO'JE&amp;&amp;:OEC+3/A&amp;(8PZL/H7EWH&lt;';[[?FJV/';VQUIM&amp;D-PM*\_+H)&gt;6+\-`E^%#$J.`H=?'Y&lt;V&lt;N\;5/9&lt;4XWR,_2#,PR?&lt;=#+Z-:\A$9N%L`!R&gt;:2WR;2T:B2]B7VB'(.#[!XJ+,&amp;6IC95O7&lt;YG.UJ&lt;Y1K-*X`:9+*'1"Y?'IU&gt;#)Z(1C$RI&lt;&gt;',5M;RHK:0IT&amp;19!H9Y#Z]9CRX(#Z=O)!6Q&amp;78PI43*7+WI&amp;P%UPGM50+)8H);63`&gt;SVA[[?]^BWDZ`"MZD6]\Z:==:R2`SJ\68V;R8Y)[VZX)N6MQ&lt;\?)[\70F?NVC.MR%^&gt;Q%P[!6"F'V[MCH7OU"`A,4J&lt;2&lt;%$.=308K$G*GF2FLF_Q=%WV*K\0HDV&lt;J-0G&lt;#RQ&lt;3.%Z4JZ,8=`&gt;Z`3`8.OCHBAAN&amp;N:YI_\*AW9@\%'-WQK;A4Z%0+]T&lt;7C1;&gt;:TOW9HF&amp;HJ/N89H?,F0^81I:1`)[R?Q_ZL+/O7S#J`"`*_;U@$P[I;P&lt;KZYL-FPF3XP6%DCB,OFD*CVU&amp;6LT:8S&lt;&lt;[8OL;;2($8&amp;N&lt;,JIXNV?05=0])=']6MAX[[\.!'[T("3Z=O99+YFEKQ!7PA9S9N&gt;#XYDVKXIJJ3"&amp;LI7NHUU&lt;U;%TSB4J\^BC&lt;3(.NBER(I5DEW9RF]T+3&amp;LI51EH5X*5XJ5:O&lt;Z@^X;MRQ8-WQXZ$B1J38SJ!52`#C3;H.$"3&amp;'&amp;$)RWS5\H+II\1+2_H9'\1&amp;?9O&gt;/$UVA![DXR2%W/F=I-X*-`H&lt;:J@FNOG?QWXD[)F(Z&amp;BP["X:&gt;%&lt;RX$`\Q(PME4Q\&amp;&gt;)J:4QYJ?J5R\R[_GUA1AW7^&lt;@LPW.:2]?_U='BF&lt;PXE^J%^PSDY@GKY@G+I&lt;99ZTEVTB%VTA]U4A0';7.R--+]R-&amp;+0;^/-[@J#G[$!^KUTLH:N*[:G='9O+K61D9_M.G3)PV&amp;]'M1!9BN"((Q'SYMML&lt;%6&lt;U;7;BU6:.V[F6^R@]_JS*G#YR6[8$:.9&gt;"6"X/QV59`=Y()L"T$AD5\*-()`(BUASYHB!$N5_)A970HY'[BW6!-$.AVRFY$6`6)BG&lt;_DX3D,)APG-54:D03U,AR0!\SE0AKY9]#**]*"K7^RQ&gt;-I.Q5Y%P-,I(8`2K$/[&lt;Y$1]$6_&lt;+D=Z/7GJ(&amp;?G=ME2@]?;N`+2;B5YJ\&lt;):7B2)^4"#DARBU$F7J4O]$OF[+(I3#DG[XKGL\9P%8YP'IP&amp;%_(YE&amp;QF^@25\Z*DI;/*QM5#ZX%L?$)ZNJ5V,'=XVG5:O#SP"VTRR?*#EV)8?&gt;B[S[GGVIM]&lt;,LGY%OV-&amp;7'X8B2P_LB&gt;X.Q\LMZ7,1&lt;@/`^SI2Z0&lt;+W3H`RT70/?F4]6:*#T0X\Q@][)/SS3HJV9)Q,_#$1EAOUAPMW:A#/WY8R"U%;))6?-HHK#^]@WR]Y_I)6K/@SV$MF/2%?DA[.2/0GTT;H!NX7_&gt;?-]S)QT`-0?KTTD][FF@-]`[$8/P]],)\Z=$XU`)-_%RA.7.)/#RAF0F&gt;X'-(1J_!#.A5&gt;P9&lt;0.`\'V&gt;(#HW8YX;+5+W=A".(`=15;J#XVPQB&gt;O(E8X]5(B22_Z?)(&lt;S/@YO_G@SX]Y3&lt;&gt;86!GL]&amp;FUC3?^`*.`Q&amp;:DB0+!!!!!!!!"!!!!%]!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!$T!!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!Q29!A!!!!!!"!!A!-0````]!!1!!!!!!J1!!!!9!#E!B"%FT4EE!!$F!&amp;A!%#U2J:WFU97QA33^0$5^T9XJJ&lt;'RP=W.P='5$2%V."F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!"2!-0````]+6G6O:'^S4G&amp;N:1!!%E!Q`````QF.&lt;W2F&lt;%ZB&lt;75!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!A1&amp;!!"1!!!!%!!A!$!!1/2'6W;7.F,GRW9WRB=X-!!!%!"1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!Z&amp;A#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!&amp;!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E7!)!!!!!!!1!&amp;!!=!!!%!!.*-%6]!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!UEQ28Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!$"&amp;A#!!!!!!!%!#!!Q`````Q!"!!!!!!#F!!!!"A!+1#%%38./31!!/5!7!!1,2'FH;82B&lt;#"*,U].4X.D?GFM&lt;'^T9W^Q:1.%45U'5G6M98FT!!!+2'6W;7.F6(FQ:1!!&amp;%!Q`````QJ7:7ZE&lt;X*/97VF!!!31$$`````#5VP:'6M4G&amp;N:1!51$$`````#U2F=W.S;8"U;7^O!#"!5!!&amp;!!!!!1!#!!-!"!Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!!1!&amp;!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;A#!!!!!!!%!"1!$!!!"!!!!!!!0!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!,Q7!)!!!!!!"A!+1#%%38./31!!/5!7!!1,2'FH;82B&lt;#"*,U].4X.D?GFM&lt;'^T9W^Q:1.%45U'5G6M98FT!!!+2'6W;7.F6(FQ:1!!&amp;%!Q`````QJ7:7ZE&lt;X*/97VF!!!31$$`````#5VP:'6M4G&amp;N:1!51$$`````#U2F=W.S;8"U;7^O!#"!5!!&amp;!!!!!1!#!!-!"!Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!!1!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"Q!-!!!!"!!!!9I!!!!I!!!!!A!!"!!!!!!'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!5]!!!)+?*S65=N/QE!5076KA3))ACAO40E"*&gt;'.OS9W*E2ZB"CWWL1$;4*WGMZ!R*8@[*@I&amp;_DNQ\BQZ:RE/P@-O???31'=QH;(ZFB.R]#VWY@:]+*VJ(XBD#^GT:E+8C-BJ!JEQJEXG6A,,PS&gt;!GS0&lt;[/!0_Q3$P4=U2=N?]HD5+:4`ZGYQY+L4W4)25[6KI&lt;(6:"'C9ZED#5_X_]\DQ!KL=,R8'Q$Y3N6$DA0N-#6/]=?;1R5Q'#?X9C.UDRV`)VS\D)RDZ-UWPI[^$7016J3WQCJYQ-7&amp;;QM=9E;[GY6&lt;#87K,FP4#ZU18#Z1JPG-&amp;)WM-]WY1IGBN2MA:LS[03%,%A,"WBH90IF*0G=9D82Q?D@-3P::G4_4^2HYJ;](04)X]JR2$=`M%L]:8ZP_DG+JRIY*E@[%43CCDJ^OTD"I-QXI+J,:U:H!_9X-=&amp;G0A!!!!"X!!%!!A!$!!5!!!"9!!]%!!!!!!]!W!$6!!!!91!0"!!!!!!0!.A!V1!!!'I!$Q1!!!!!$Q$9!.5!!!"T!"'%!)!!!"%!\Q$E!!!!&gt;9!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!4!!5F.31QU+!!.-6E.$4%*76Q!!&amp;R!!!!2P!!!!)!!!&amp;P!!!!!!!!!!!!!!!#!!!!!U!!!%:!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B$1V.5!!!!!!!!!:R-38:J!!!!!!!!!&lt;"$4UZ1!!!!!!!!!=2544AQ!!!!!1!!!&gt;B%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!A!!!CBW:8*T!!!!"!!!!G241V.3!!!!!!!!!MB(1V"3!!!!!!!!!NR*1U^/!!!!!!!!!P"J9WQY!!!!!!!!!Q2$5%-S!!!!!!!!!RB-37:Q!!!!!!!!!SR'5%BC!!!!!!!!!U"'5&amp;.&amp;!!!!!!!!!V275%21!!!!!!!!!WB-37*E!!!!!!!!!XR#2%BC!!!!!!!!!Z"#2&amp;.&amp;!!!!!!!!![273624!!!!!!!!!\B%6%B1!!!!!!!!!]R.65F%!!!!!!!!!_")36.5!!!!!!!!!`271V21!!!!!!!!"!B'6%&amp;#!!!!!!!!""Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"A!!!!!!!!!!0````]!!!!!!!!!P!!!!!!!!!!!`````Q!!!!!!!!$1!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!""!!!!!!!!!!!`````Q!!!!!!!!%-!!!!!!!!!!(`````!!!!!!!!!41!!!!!!!!!!0````]!!!!!!!!"4!!!!!!!!!!!`````Q!!!!!!!!'=!!!!!!!!!!$`````!!!!!!!!!;Q!!!!!!!!!!@````]!!!!!!!!$3!!!!!!!!!!#`````Q!!!!!!!!2!!!!!!!!!!!4`````!!!!!!!!"7Q!!!!!!!!!"`````]!!!!!!!!&amp;@!!!!!!!!!!)`````Q!!!!!!!!7-!!!!!!!!!!H`````!!!!!!!!":Q!!!!!!!!!#P````]!!!!!!!!&amp;L!!!!!!!!!!!`````Q!!!!!!!!7]!!!!!!!!!!$`````!!!!!!!!"&gt;1!!!!!!!!!!0````]!!!!!!!!&amp;[!!!!!!!!!!!`````Q!!!!!!!!:M!!!!!!!!!!$`````!!!!!!!!#H!!!!!!!!!!!0````]!!!!!!!!+?!!!!!!!!!!!`````Q!!!!!!!!K)!!!!!!!!!!$`````!!!!!!!!%)1!!!!!!!!!!0````]!!!!!!!!1D!!!!!!!!!!!`````Q!!!!!!!"#5!!!!!!!!!!$`````!!!!!!!!%+1!!!!!!!!!!0````]!!!!!!!!2$!!!!!!!!!!!`````Q!!!!!!!"%5!!!!!!!!!!$`````!!!!!!!!&amp;/1!!!!!!!!!!0````]!!!!!!!!5\!!!!!!!!!!!`````Q!!!!!!!"4U!!!!!!!!!!$`````!!!!!!!!&amp;3!!!!!!!!!!A0````]!!!!!!!!7&gt;!!!!!!+2'6W;7.F,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!1Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!&amp;!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!9!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!!Z!-0````]&amp;47^E:7Q!#E!B"%FT4EE!!&amp;9!]&gt;*,JL5!!!!#$E2F&gt;GFD:3ZM&gt;G.M98.T#E2F&gt;GFD:3ZD&gt;'Q!.%"1!!5!!!!"!!)!!Q!%(E.M&gt;8.U:8)A986T)%NM98.T:7ZQ=GFW982E982F&lt;A!!!1!&amp;!!!!"@``````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!A!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!!J!)12*=UZ*!!!?!$@`````!!E7!)!!!!!!!1!%!!!!!1!!!!!!!!!U1(!!&amp;12598.L!!!"!!5&amp;4EF%16%7!)!!!!!!!1!%!!!!!1!!!!!!!!B3:8.P&gt;8*D:1!!7!$RUEO^0!!!!!)/2'6W;7.F,GRW9WRB=X-+2'6W;7.F,G.U&lt;!!W1&amp;!!"A!!!!%!!A!$!!1!"BZ$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"Q!!!!9!!!!!!!!!!1!!!!)!!!!$!!!!"0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!$!!!!!!A!#E!B"%FT4EE!!"Y!.`````]!#29!A!!!!!!"!!1!!!!"!!!!!!!!!$2!=!!6"&amp;2B=WM!!!%!!16/352"529!A!!!!!!"!!1!!!!"!!!!!!!!#&amp;*F=W^V=G.F!!!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!"2!-0````],2'6T9X*J=(2J&lt;WY!7!$RUEP"&lt;A!!!!)/2'6W;7.F,GRW9WRB=X-+2'6W;7.F,G.U&lt;!!W1&amp;!!"A!!!!)!!Q!%!!5!"BZ$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"Q!!!!9!!!!%!!!!"1!!!!%!!!!#!!!!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!%!!!!!!9!#E!B"%FT4EE!!$F!&amp;A!%#U2J:WFU97QA33^0$5^T9XJJ&lt;'RP=W.P='5$2%V."F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!"2!-0````]+6G6O:'^S4G&amp;N:1!!%E!Q`````QF.&lt;W2F&lt;%ZB&lt;75!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A"7!0(34"&amp;@!!!!!AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QJ%:8:J9W5O9X2M!$2!5!!&amp;!!!!!1!#!!-!""Z$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"1!!!!5!!!!!!!!!!A!!!!-!!!!%!!!!"1!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.4</Property>
	<Item Name="Device.ctl" Type="Class Private Data" URL="Device.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="DataOperation" Type="Folder">
		<Item Name="Description" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Description</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Description</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readDescription.vi" Type="VI" URL="../readDescription.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],2'6T9X*J=(2J&lt;WY!,%"Q!"Y!!"!/2'6W;7.F,GRW9WRB=X-!!""%:8:J9W5A+%&amp;V=W&gt;B&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeDescription.vi" Type="VI" URL="../writeDescription.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#U2F=W.S;8"U;7^O!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="DeviceType" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DeviceType</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DeviceType</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readDeviceType.vi" Type="VI" URL="../readDeviceType.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$F!&amp;A!%#U2J:WFU97QA33^0$5^T9XJJ&lt;'RP=W.P='5$2%V."F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!M1(!!(A!!%!Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!%%2F&gt;GFD:3!I27FO:W&amp;O:SE!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeDeviceType.vi" Type="VI" URL="../writeDeviceType.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!M1(!!(A!!%!Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!%%2F&gt;GFD:3!I27FO:W&amp;O:SE!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="IsNI" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">IsNI</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">IsNI</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readIsNI.vi" Type="VI" URL="../readIsNI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!)12*=UZ*!!!M1(!!(A!!%!Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!%%2F&gt;GFD:3!I186T:W&amp;O:SE!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,%"Q!"Y!!"!/2'6W;7.F,GRW9WRB=X-!!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeIsNI.vi" Type="VI" URL="../writeIsNI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!+1#%%38./31!!,%"Q!"Y!!"!/2'6W;7.F,GRW9WRB=X-!!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="Model" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Model</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Model</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readModelName.vi" Type="VI" URL="../readModelName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]&amp;47^E:7Q!,%"Q!"Y!!"!/2'6W;7.F,GRW9WRB=X-!!""%:8:J9W5A+%&amp;V=W&gt;B&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeModelName.vi" Type="VI" URL="../writeModelName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1$$`````"5VP:'6M!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="VendorName" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VendorName</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VendorName</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readVendorName.vi" Type="VI" URL="../readVendorName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````]+6G6O:'^S4G&amp;N:1!!,%"Q!"Y!!"!/2'6W;7.F,GRW9WRB=X-!!""%:8:J9W5A+%&amp;V=W&gt;B&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeVendorName.vi" Type="VI" URL="../writeVendorName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#F:F&lt;G2P=EZB&lt;75!!#R!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="DeviceType.ctl" Type="VI" URL="../DeviceType.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!\!!!!!1!T1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!2&amp;&lt;H6N!!!"!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
	</Item>
</LVClass>
