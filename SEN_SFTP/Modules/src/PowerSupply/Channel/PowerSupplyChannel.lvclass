﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.4</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#,L5F.31QU+!!.-6E.$4%*76Q!!(MQ!!!1@!!!!)!!!(KQ!!!!@!!!!!2J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;A#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!1!!1!'`````Q!!!!!!!!!!!!!!!$D5U&gt;U6"&gt;N$K2`J"U?D9&gt;A!!!!-!!!!%!!!!!$.&gt;2NU4&amp;241+EU6IKUS&lt;46V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!NI[Y2"+RDU_X14#K;%B*WA%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"!^HF[Q-:;P*&gt;41&amp;&amp;`(D::P!!!!"!!!!!!!!!#-!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;5.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;&amp;2Z='6T,G.U&lt;&amp;"53$!!!!!R!!%!"A!!!!N1&lt;X&gt;F=F.V=("M?1&gt;5?8"F2'6G&amp;5.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;&amp;2Z='6T,G.U&lt;!!!!!!!!5)!!!!!!!-!!!!#!!)!!!!!!#U!!!!W?*RD%'"A&lt;G#YQ!$%D%$-V-!&amp;:$0^9!$4$"]9"$B!MD!3!BE9!*#&gt;$.5!!!!!!!",!!!"'(C=9W$!"0_"!%AR-D!QKQ&amp;J.D2R-!VD5R0A-B?886"R:C"G!7*7C#AD5)RJ$Z$"""+(KN'&amp;S$(^!/)4[/;QI1M!!1$MPC@5!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!*U!!!%A(C=F640;R."&amp;*Z*BD"+90913G[+\C')B"3,#/EBM1G77'FCKU*&amp;+[ABAK"I)C8VR]J;=6SX;.(CS@^!]"2+$Z*UC[FYC3A?"%M&amp;"35^3,QIC0(.\'R-2,!/\(P@@$0P?W^HXWT-D^#:88NW'_$^'+%_:+#4ZU[&gt;XIRADNR2)/C`2V&lt;&amp;\`C$\[][?2B2NO7'H*PL13OF;_:F0?QLB;Q20=2(&gt;-+*`OF(O^UWFY//.8SWHBQ5?SU+@$WF5T%",]NKZQJCF[?8"46K4'V(2&gt;*@H8$KRA"W%XO3%\XZ$`'09U=/G_P53GN7C@+&lt;&amp;["K[U#)TVY3!&amp;G&lt;?OLZBNH^+JOZ#++7X'I0%TYL9_:&amp;A&amp;UG`+E!,;9F#*A-S&lt;0I]_E!W'JZ;ZZ&amp;'CWW^",FW&gt;7'O9;"L&lt;':#+CRW^P!WDH+Z[8M*/70F%[3ABGF`^::1U,H,6A\1^WK\'H;K7?PU.G`!:W(5G=/&lt;&lt;C?].^U3O0FAX9:UINYXL$(1%K?;9,'BSA=9SFO\S0OA&gt;M:YB[Y53;)06ASLAHHX&amp;K.P9G^+I::Z:HZ"4@^L&amp;)VF_HRS;0(()$.LX97K`*SW"0(];4]2J76"@(F7&lt;L7@.(4&lt;R3;4+O^*^&lt;VC(D$?])#&amp;?4D.&amp;R0U:V^--=L(.K,"\UOJ*X_EKV3V$5_)$K2C"2SDR7I*S+S,R-BVR6=._7[]])NS!LA$:T/@8D=8&amp;U5@?TLOC-R?-4_+-))ST7-LKCV1=!_R8FX3_!H3O"T6]SC7I^U=;^^PX-)+&amp;&lt;O+%\A/96&amp;`I$CXA&amp;X6_%0A%]IX!2M_&amp;T=!K]J`DPAKO*`+DQUGEJX`Q?]@]IP+BA3\1!!!!Q7!)!:!!!%-49O-!!!!!!-&amp;A#!!!!!"$%W,D!!!!!!$"9!A"E!!!1R.CYQ!!!!!!Q7!)!!!!!%-49O-!!!!!!-&amp;A#!'1!!"$%W,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!O&lt;E!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!O&gt;(&amp;S^'Z!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!O&gt;(&amp;P\_`P]P2O1!!!!!!!!!!!!!!!!!!!!$``Q!!O&gt;(&amp;P\_`P\_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!0``!-P&amp;P\_`P\_`P\_`P\_`S^%!!!!!!!!!!!!!!!!!``]!R=7`P\_`P\_`P\_`P\``SQ!!!!!!!!!!!!!!!!$``Q$&amp;S]P&amp;P\_`P\_`P\`````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,R&lt;_`P\```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]82````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!S]P,S]P,S]P````````,SQ!!!!!!!!!!!!!!!!$``Q!!R=8,S]P,S``````,U=5!!!!!!!!!!!!!!!!!!0``!!!!!-8,S]P,```,S]5!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!$&amp;S]P,S\]!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!R&lt;]!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!)1!!5:13&amp;!!!!!"!!*52%.$!!!!!26$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'R16%AQ!!!!-1!"!!9!!!!,5'^X:8*4&gt;8"Q&lt;(E(6(FQ:52F:B6$&lt;WZU=G^M28:F&lt;H25?8"F=SZD&gt;'Q!!!!!!!"#!!!!!!!!!!%!!!#B5&amp;2)-!!!!!!!!!!!!!-!!!HF!!!K-XC=T:J`=&amp;28&amp;=@0W\S1N]H3P%VW;69B_Y/81'-4NF"+I*23]EC;1..K#N8]16CSSY]UT=:.5KNI%_IGEL:)+Z+/N7A&gt;E6;L&lt;27:IM&lt;;;IS/KR&lt;16I4K5M&lt;J&gt;-2K`[FF+,PLO@@^W0@W&gt;X[:--/&gt;H?7?=_]^Z`-^^Q=,5,7-,T.%Y=%)-0T\_'&amp;T"!K^)1:AL*I$_5`F%0"&lt;G!_"M&gt;C9#+THNP$H$6&amp;G912-XF!FZR;(Y4XM(&lt;M1_QI=:BD_,?Q[D\?BM]))&amp;(N$VZK&lt;B$!PP,"1'-Z8P*JB%8_!C2I_+&gt;A`Y!Y%`4AA"*?3VFT.2)%2H3Q&lt;&gt;.XF[@)&amp;"@+NM:KT5:@'#0"C;&amp;&amp;!##^"DTDU,[F,:BM=:@S+3U#83_(Y]?.R)\.E6%GH55NMDK,&lt;%]SW$$9F;.-DB+_H.I85"M=ZI9QDWK/(S.S*5;*JK2DCU24N(F#7@&amp;1:3\2(VF/\CR=PIBWWMNXO#&amp;C%]!&lt;/RLXF0:=``_0"FY!"*HAKNBH4U=&lt;&amp;H.&amp;R&gt;DF\S&gt;R-=E%N3D!&gt;&amp;7M9PA5`NU3A+BAS&lt;!&gt;7(J%:BE-U';S3D,79$+;?*M-I$P-1T];B,.E1W4(8ILL/PJZ?8]$BX_FI\`4U^$C[!XPO^`4[(&amp;Z0LS=Z4\?))?-K%A-S'%5%L-!SA^#FD&lt;E@DBU\BG(!.G[[$EWP&amp;=+KH:GO*K$'X2O0/REV(L^&lt;-8\CZ&lt;NXE2B7L$%IZ(+5X#/U&gt;&gt;/WD&lt;9&gt;N/WD\1"N([(NVDDDSZ&amp;R-Z_Y!"XD+[?@]:M1P1=4')=/JBPW:O"VF715:RT\!\-0/D,9V+*.PZ:RN/F!G\X:'6_&gt;R$CR47"]:'2%:Y=Z7K-STD+-SPD6W&amp;8#_,/R@T+H9$^FH+-77T!&gt;9MA"=AK9&amp;4B/'\4KS&gt;[7EGQ'=^#;/1?/?3$286Q@](@VEGRU&lt;LT@V^7&lt;H*0N9IB&lt;+91:()+Y,YZ^#-PAI]J[`\%K/&lt;C\*159:94B?L#K9@V8SL"C?0:1B/@PF2$/)QD,]L\8'^L/MVJ.,))$_DA=CK/IR#(W(UKD-3O,AAO3&amp;`V6-72&lt;I2=RCRLA9$CLC!_D;;F'R#1B*JRQ6B'0[%4-+C*GK43@I/U*WLZ+W^.RM@93M6I3*[I4;``UCX6@+L%/-CS-:"$?=**979S0&amp;19TW$S=*&amp;97R\%KYW13[[0*9G76M4+*^@%-9IU_%\N+`F+RMIJ9HR"$&lt;E7MN-=\M)_GI%"*Q&gt;&gt;)#D;E%ONAVBS-O3SXV^V26_O_=55&gt;[D8A4[@7*]61W1W37O5U7S!0&lt;I4&amp;KG"8*A@Y[Z+2*WZ5$INB,&gt;3F3UJ2"*[3&amp;+,&gt;K_!WW+R.A^G/ST@Q.F8[.O6TD/&gt;M2HN]!N^!:T@LZ&lt;9!]H%'$=KU9_:U+@YG4H[V%,&lt;&amp;*W_/P1VO(&amp;C?`,`4F:SHJ:,4,AEO8SEZXAB]#\&amp;:K]T#$1&gt;J)P/52(Z&lt;U:,YALV*+4@P12(G]G#/:4?8&lt;/)-DUITX#2`=U]%PO-.M4S)98O4)O'0Q-.[TJYBUWP7&amp;U.J&gt;J:*&amp;--,%8A7R\4B?AMVY"&lt;!IV!'4S7EZ]S:-RDE+V?O3"[$4BBA$5'"@'/M$P:7V,BXSG\H2_#\G04F1NCE38I_&amp;'0)BX,QSCJ?RWLSW::?@\@MVR3"\[(@GY3QA@JVK[=^%ZD1V_DI+0L#.AZHHG0-(O`CI&amp;W=J/6&gt;-LHNH%NC.\%LK2R/UOK\KGDD&lt;*[46JGHG=UV.'O4HMW/X'?T1T=&lt;&amp;-PX=4&lt;;%S;$8"#!^*M4$&lt;B_=`I"EFDR+;BI"&gt;SC@**CZF5/'3I&gt;5/G-6&lt;L!@!F8!-:,UD]6I*A',;`P@\(U\9.=C_Q'K_1*&lt;:6]"QPK&gt;?#A^/9L^,[5NEJ;V,*^/"W_IOP/LO3;?&amp;)K&lt;SXS#;9=$,!/8"FKWU^3VD:B=L8NJ_FKGTV,&lt;=/I`UT3`^V35$GF1NU:A6&amp;3I&lt;4_VI&amp;.8[6?*I(=2!.J6=L!OQ#R+.4EM.U5&lt;0$\/XU?/:J]B84XQH](93G$U[F1!YS-`Q+8S!NBK_&lt;)!UP"EB1P6E]N[?+A8:SEF;A6\$^0ZDN.TZ&gt;T\DE[M:ZK`D!(L^!=G*:)/4"C$I;W%7-NVK^KM)Z&gt;BVBX1+U?[V_FR8JN&gt;KT(8)6V@9%!&lt;B%NPB2\`NB%_@\V&gt;0)^0A7_@[0DOV$B_[Y)`.9&lt;GK=(`'/Q4L`0`3\&amp;I@^&gt;+%$!;X-"P,HP0F^A4XPS&gt;B=CW^VS:&lt;O4*G!!)Z4#+M8P@`5,3NDN]"O[WSVOWR70_?_F@5$;F=L7+T)JB)*5!9^V;Q,?K1EY?PK$&gt;E?R$""0B?D*GM;48_0J8LWH0[)HP*973Q(EJ-O$%8AIVHD+6[QPO_/?,C`4@+\2@+[/@`\AN"[5VZ)O2A5Y[&gt;7[8+7#Z"3&amp;J/CE"%E2&lt;DLD$H+CI8`'H6S`]N'F0NVJN8F;K]U/V/:T=,.?GX^+K]V&lt;=^&amp;GU6:`:[^HF]_25JR`HKAY8Z^/=&lt;YR"8(_23&gt;/EU;=:\/,]^Q-C@0]D)DTT7E4Z^_G4:R``\_+-TR*=6\1C8._LO)U.GM5Q*U@\_/C*`(/D(^6[?)BZR&amp;SVT?A&amp;&amp;&gt;!F@\"ZQ"Z_'L1(7\E^YT3H'ZA+1[+?)\ZMBASFGD/-1PQ[FQ$3X$FZ:P1$7F4H70+5*U/WM6*7P8UX:"][*#[?G/E+\&lt;:OU\&gt;K`9S=F"[G&lt;O(2EV;9SH;XQ"6WC?J6'MMRS\S;9E?#*5BP-GTG97OWP0;9ZK&lt;SC?M%J@83+=WD*0=$YP9!"9RW%"#="9*-U#4PI!^F0*JN1A*;]LB0QW-&gt;&lt;M^86W_TG:`=AH\)J9Q9U)*M_,&amp;NR*O33D+?-(.]4K.O1VC&lt;B/OP4S/P("WLLW$/*O%;W]ZWF@.QL58;_G1LJ;_%I_Y&amp;7@TVX.PYGT[BJ[?XIL[J;4`T#H#Z&gt;_7`2VYPR&lt;A)AHA9CSM1YY"\$@E\)`&amp;S)5\4H+@3P,TW/%M&gt;/B*`ER;EDNS@+UKFG]@&gt;`A]08U"HT?:[1?GD?HYPIQ5@8:/-@WZ/=8UXFFA_P/4:@I,+:DG=TIMK*2XK*1(]@PHI6Z0?7&gt;;SONTJ$T^(2M"PW^G!/_;5Y$\ZR4AX&lt;-!_+=H#XAA"?$GC1(?JA,?3BB.,//?K:&gt;R_;+;PITPG"H+W_=5Z&gt;YZ2&lt;FP&amp;CD@/6H+&gt;[7AP'2CF,N6SNXY@3MU[CE8UV,?G#0F':ZDE0#.-U.Y`:QCP'&amp;/%8\\,"$?/&amp;H#GV)18DI"QL&gt;%Y)DG:TPU6SBO;.!`/,[9^G=\$6E?(&amp;8+4@2X/^XE&gt;TN&lt;'Z.@(8]IBLA*P4L_30\.TL3]/B[@QKPDDX7PDB;]O+&gt;[,6+DXC+)M5Q&gt;_)XIND]#6L'OZ$7_%6&gt;AYBKZD@Q21Z2B)L#!/]+^8S%S9W_I0_1=W[Q;"U^"!RC%*_V=W@]!06`.JA!!!!!!!!1!!!#_!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!"R1!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!3M7!)!!!!!!!1!)!$$`````!!%!!!!!!1]!!!!+!!1!!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!%!!A!!$%:S&lt;WZU='&amp;O:7R731!!%5!+!!N7&lt;WRU97&gt;F)&amp;.F&gt;!!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!!21!I!#E.V=H*F&lt;H24:81!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!!^!!Q!*1WBB&lt;GZF&lt;%ZP!!B!)1*0&lt;A!!-%"1!!=!!A!$!!1!"1!'!!=!#"J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!!1!*!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!%57!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!A!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'29!A!!!!!!"!!5!"Q!!!1!!V9CYI1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;A#!!!!!!!%!"1!(!!!"!!$6C,CB!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!BM7!)!!!!!!!1!)!$$`````!!%!!!!!!@]!!!!3!!1!!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!%!!A!!$%:S&lt;WZU='&amp;O:7R731!!%5!+!!N7&lt;WRU97&gt;F)&amp;.F&gt;!!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!!21!I!#E.V=H*F&lt;H24:81!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!!^!!Q!*1WBB&lt;GZF&lt;%ZP!!B!)1*0&lt;A!!21$RV9CQ%Q!!!!%61W^O&gt;(*P&lt;%6W:7ZU6(FQ:8-O9X2M!#&gt;!&amp;A!""&amp;.U&lt;X!!!"2)1UV$/$!U-U.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;!!!)%"Q!"E!!1!*&amp;%B$45-Y-$1T1W^O&gt;(*P&lt;%6W:7ZU!!!%!#%!%E"Q!!A!!1!,!!A!!!*0&lt;A!!"1!+!!!;1(!!#!!"!!U!%A!!#E.V=H*F&lt;H24:81!!"J!=!!)!!%!$1!3!!!,6G^M&gt;'&amp;H:3"4:81!/E"Q!"=!!!!%!!%!!!0I!!I!!1RG0!!!$!!"$'9]!!!/!!%-:DQ!!!]02H*P&lt;H2197ZF&lt;%6W:7ZU!$*!5!!)!!)!!Q!%!!5!"A!(!!A!%"J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!!1!2!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;A#!!!!!!!%!"1!$!!!"!!!!!!!N!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!D17!)!!!!!!%A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(%"Q!!A!!1!"!!)!!!R'=G^O&gt;("B&lt;G6M6EE!!"&amp;!#A!,6G^M&gt;'&amp;H:3"4:81!&amp;5!+!!^7&lt;WRU97&gt;F476B=X6S:71!%5!+!!J$&gt;8*S:7ZU5W6U!!!61!I!$U.V=H*F&lt;H2.:7&amp;T&gt;8*F:!!01!-!#5.I97ZO:7R/&lt;Q!)1#%#4WY!!%5!]&gt;7)M"-!!!!"&amp;5.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;&amp;2Z='6T,G.U&lt;!!H1"9!!124&gt;'^Q!!!53%..1TAQ.$.$&lt;WZU=G^M28:F&lt;H1!!#"!=!!:!!%!#22)1UV$/$!U-U.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;!!!"!!B!"*!=!!)!!%!#Q!)!!!#4WY!!!5!#A!!'E"Q!!A!!1!.!")!!!J$&gt;8*S:7ZU5W6U!!!;1(!!#!!"!!U!%A!!#V:P&lt;(2B:W5A5W6U!$J!=!!8!!!!"!!"!!!$[!!+!!%-:DQ!!!Q!!1RG0!!!$A!"$'9]!!!0$U:S&lt;WZU5'&amp;O:7R&amp;&gt;G6O&gt;!!S1&amp;!!#!!#!!-!"!!&amp;!!9!"Q!)!"!;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!!%!%1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!"1!%Q!!!!1!!!$A!!!!+!!!!!)!!!1!!!!!"A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*&amp;!!!%ZXC=F60.5N.1&amp;0\3N03(1GNJKV416&amp;25M.;""?/YS*C"U181M1RL-_UN-B/44(J49?@3V`!.80ICPI.,@!/`G[2#F1[1-Z0=H.`P@/&gt;=!#_RD&gt;]`PXZ@!+$6,-_6A?&gt;MDY1L$UZ^-7TVJ).6MQYNX:7?$V4@7LP76HNTY[)LE'&lt;Y!=Z1B$:&amp;A"0=`&amp;ES@?19H;+AO"/QK'_\QDF]"^QS#ZA^^"RJ(QGD+S2K6*13R;[QBW%A_J&amp;8Q1K$A$C65_S6+0Z[F5Q&gt;??OD\4,XHI?=W5TNOY$"]IM+`.3WG[AE%'@Z2B3611&amp;I*/IZ6$!*9-)SU=!L7GZ$J76*`2@4;-8";SB7I_^]]CW6)C9[CIE93;#'_/-&lt;9V/.DP&gt;:".X1^ZX4J+/7-_IZ^H"9P]3E"LRF&gt;IB):^U-:J$F/9`SMO7%1SE#QRM95&lt;4B"]=D7QKD&lt;UO&lt;HBHWB4\8ZIS.:"A?`W)$6?4.,03"=U1GP_D??RELB$&gt;!G&lt;6UV.HH(4XM$^D4B1HD';\*!&amp;F5G"NE-:9K"X"8FS&gt;^RH39;R&amp;,00793W?O]2ZJF_T2SN8L0&gt;Y$9YL^X]%VLTWYT;C._R050\C#_LANV5R'N;-#_7,F($Z%)(&lt;)A)&amp;(?-T"+&amp;EF/W/J*0+`ZNTS***YGBK?*C"PNB]R3%8)0.'E_6`EK=2*H&lt;/.INH-PP%]2^AOZHAPMXPB*R%=^YC=^RM,K'%:^\##B^S-.;Q4QTJT2@7:?9V3RH0G&lt;KFE?-%+&lt;&gt;:IUZYDTBHC450`!T%_V5!!!!!!!!"X!!%!!A!$!!5!!!"9!!]%!!!!!!]!W!$6!!!!91!0"!!!!!!0!.A!V1!!!'I!$Q1!!!!!$Q$9!.5!!!"TA!#%!)!!!!]!W!$6!!!!&gt;9!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!4%!5F.31QU+!!.-6E.$4%*76Q!!(MQ!!!1@!!!!)!!!(KQ!!!!!!!!!!!!!!#!!!!!U!!!%#!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R$1V.5!!!!!!!!!:"-38:J!!!!!!!!!;2$4UZ1!!!!!!!!!&lt;B544AQ!!!!!!!!!=R%2E24!!!!!!!!!?"-372T!!!!!!!!!@2735.%!!!!!!!!!ABW:8*T!!!!"!!!!BR41V.3!!!!!!!!!I"(1V"3!!!!!!!!!J2*1U^/!!!!!!!!!KBJ9WQY!!!!!!!!!LR-37:Q!!!!!!!!!N"'5%BC!!!!!!!!!O2'5&amp;.&amp;!!!!!!!!!PB75%21!!!!!!!!!QR-37*E!!!!!!!!!S"#2%BC!!!!!!!!!T2#2&amp;.&amp;!!!!!!!!!UB73624!!!!!!!!!VR%6%B1!!!!!!!!!X".65F%!!!!!!!!!Y2)36.5!!!!!!!!!ZB71V21!!!!!!!!![R'6%&amp;#!!!!!!!!!]!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!0````]!!!!!!!!!S!!!!!!!!!!!`````Q!!!!!!!!$=!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!"&gt;!!!!!!!!!!!`````Q!!!!!!!!&amp;]!!!!!!!!!!$`````!!!!!!!!!&lt;!!!!!!!!!!!0````]!!!!!!!!#!!!!!!!!!!!!`````Q!!!!!!!!)1!!!!!!!!!!4`````!!!!!!!!")A!!!!!!!!!"`````]!!!!!!!!%G!!!!!!!!!!)`````Q!!!!!!!!3I!!!!!!!!!!H`````!!!!!!!!",A!!!!!!!!!#P````]!!!!!!!!%S!!!!!!!!!!!`````Q!!!!!!!!49!!!!!!!!!!$`````!!!!!!!!"0!!!!!!!!!!!0````]!!!!!!!!&amp;"!!!!!!!!!!!`````Q!!!!!!!!7)!!!!!!!!!!$`````!!!!!!!!#9Q!!!!!!!!!!0````]!!!!!!!!+&amp;!!!!!!!!!!!`````Q!!!!!!!"1!!!!!!!!!!!$`````!!!!!!!!&amp;!A!!!!!!!!!!0````]!!!!!!!!5%!!!!!!!!!!!`````Q!!!!!!!"1A!!!!!!!!!!$`````!!!!!!!!&amp;)A!!!!!!!!!!0````]!!!!!!!!5E!!!!!!!!!!!`````Q!!!!!!!"OI!!!!!!!!!!$`````!!!!!!!!'\!!!!!!!!!!!0````]!!!!!!!!&lt;O!!!!!!!!!!!`````Q!!!!!!!"PE!!!!!!!!!)$`````!!!!!!!!(D!!!!!!&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!&amp;!!%!!!!!!!!!!!!!!1!C1&amp;!!!"J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!%!!1!!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!%!!A!!$%:S&lt;WZU='&amp;O:7R731!!:!$RV9BFZ!!!!!);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,G.U&lt;!!K1&amp;!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!(`````!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!I!"!!!!&amp;1!]!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!!1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!!21!I!#V:P&lt;(2B:W5A5W6U!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!&amp;5!+!!^$&gt;8*S:7ZU476B=X6S:71!$U!+!!F$;'&amp;O&lt;G6M4G]!#%!B!E^O!!"Q!0(6C'&gt;W!!!!!BJ1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=R:1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO9X2M!$:!5!!(!!)!!Q!%!!5!"A!(!!A&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!*!!!!"Q!!!!$```````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!!!!1!!!!!!!Q!!!!!+!!1!!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!%!!A!!$%:S&lt;WZU='&amp;O:7R731!!%5!+!!N7&lt;WRU97&gt;F)&amp;.F&gt;!!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!!21!I!#E.V=H*F&lt;H24:81!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!!^!!Q!*1WBB&lt;GZF&lt;%ZP!!B!)1*0&lt;A!!=!$RV9BTU!!!!!);5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,G.U&lt;!!W1&amp;!!"Q!#!!-!"!!&amp;!!9!"Q!)(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#1!!!!=!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!7!)!!!!!!!!!!!!!!!!!!!1!!!!!!"!!!!!!3!!1!!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!%!!A!!$%:S&lt;WZU='&amp;O:7R731!!%5!+!!N7&lt;WRU97&gt;F)&amp;.F&gt;!!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!!21!I!#E.V=H*F&lt;H24:81!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!!^!!Q!*1WBB&lt;GZF&lt;%ZP!!B!)1*0&lt;A!!21$RV9CQ%Q!!!!%61W^O&gt;(*P&lt;%6W:7ZU6(FQ:8-O9X2M!#&gt;!&amp;A!""&amp;.U&lt;X!!!"2)1UV$/$!U-U.P&lt;H2S&lt;WR&amp;&gt;G6O&gt;!!!)%"Q!"E!!1!*&amp;%B$45-Y-$1T1W^O&gt;(*P&lt;%6W:7ZU!!!%!#%!%E"Q!!A!!1!,!!A!!!*0&lt;A!!"1!+!!!;1(!!#!!"!!U!%A!!#E.V=H*F&lt;H24:81!!"J!=!!)!!%!$1!3!!!,6G^M&gt;'&amp;H:3"4:81!/E"Q!"=!!!!%!!%!!!0I!!I!!1RG0!!!$!!"$'9]!!!/!!%-:DQ!!!]02H*P&lt;H2197ZF&lt;%6W:7ZU!()!]&gt;7)O+%!!!!#'F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZD&gt;'Q!/%"1!!A!!A!$!!1!"1!'!!=!#!!1(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!%1!!!!A!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"9!A!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="PowerSupplyChannel.ctl" Type="Class Private Data" URL="PowerSupplyChannel.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="FrontPanel" Type="Folder">
		<Item Name="DefaultFP.vi" Type="VI" URL="../DefaultFP.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"A!!!!!A!%!!!!6!$Q!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Y!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
	</Item>
	<Item Name="Properties" Type="Folder">
		<Item Name="ChannelNo" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ChannelNo</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ChannelNo</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read ChannelNo.vi" Type="VI" URL="../Properties/Read ChannelNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!!Q!*1WBB&lt;GZF&lt;%ZP!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="Write ChannelNo.vi" Type="VI" URL="../Properties/Write ChannelNo.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!01!-!#5.I97ZO:7R/&lt;Q!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
		</Item>
		<Item Name="CurrentMeasured" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">CurrentMeasured</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">CurrentMeasured</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read CurrentMeasured.vi" Type="VI" URL="../Properties/Read CurrentMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!#A!01X6S=G6O&gt;%VF98.V=G6E!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="Write CurrentMeasured.vi" Type="VI" URL="../Properties/Write CurrentMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$U.V=H*F&lt;H2.:7&amp;T&gt;8*F:!!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
		<Item Name="CurrentSet" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">CurrentSet</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">CurrentSet</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read CurrentSet.vi" Type="VI" URL="../Properties/Read CurrentSet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!+1X6S=G6O&gt;&amp;.F&gt;!!!0E"Q!"Y!!"Q;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="Write CurrentSet.vi" Type="VI" URL="../Properties/Write CurrentSet.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#E.V=H*F&lt;H24:81!!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
		<Item Name="FrontPanel" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">FrontPanel</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">FrontPanel</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read FrontpanelVI.vi" Type="VI" URL="../Properties/Read FrontpanelVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'H!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;1!]!!-!!1!"!!%!!1!"!!%!!1!"!!%!!1!"!!%!!!!?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"R!=!!)!!%!"1!#!!!-2H*P&lt;H2Q97ZF&lt;&amp;:*!!!_1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!"Q;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!"61&lt;X&gt;F=F.V=("M?5.I97ZO:7QA;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1351098880</Property>
			</Item>
			<Item Name="Write FrontpanelVI.vi" Type="VI" URL="../Properties/Write FrontpanelVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'H!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"5!0!!$!!%!!1!"!!%!!1!"!!%!!1!"!!%!!1!"!!!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=1(!!#!!"!!=!!A!!$%:S&lt;WZU='&amp;O:7R731!!0%"Q!"Y!!"Q;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!"61&lt;X&gt;F=F.V=("M?5.I97ZO:7QA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*)!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
		<Item Name="On" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">On</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">On</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read On.vi" Type="VI" URL="../Properties/Read On.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!B!)1*0&lt;A!!0E"Q!"Y!!"Q;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="Write On.vi" Type="VI" URL="../Properties/Write On.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!)1#%#4WY!!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
		<Item Name="Voltage Set" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Voltage Set</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Voltage Set</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Voltage Set.vi" Type="VI" URL="../Properties/Read Voltage Set.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!,6G^M&gt;'&amp;H:3"4:81!0E"Q!"Y!!"Q;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!":1&lt;X&gt;F=F.V=("M?5.I97ZO:7QA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="Write Voltage Set.vi" Type="VI" URL="../Properties/Write Voltage Set.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#V:P&lt;(2B:W5A5W6U!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
		<Item Name="VoltageMeasured" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VoltageMeasured</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VoltageMeasured</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read VoltageMeasured.vi" Type="VI" URL="../Properties/Read VoltageMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!#A!06G^M&gt;'&amp;H:5VF98.V=G6E!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="Write VoltageMeasured.vi" Type="VI" URL="../Properties/Write VoltageMeasured.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$V:P&lt;(2B:W6.:7&amp;T&gt;8*F:!!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Close.vi" Type="VI" URL="../Close.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$Z!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!"Q;5'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M,GRW9WRB=X-!!"61&lt;X&gt;F=F.V=("M?5.I97ZO:7QA;7Y!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!9#!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!(!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="Configure.vi" Type="VI" URL="../Configure.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!I!$%.V=H*F&lt;H2-;7VJ&gt;!!!$5!+!!&gt;7&lt;WRU97&gt;F!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!%!!!!"!!!!#3!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Init.vi" Type="VI" URL="../Init.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%X!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Measure.vi" Type="VI" URL="../Measure.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!+!!&gt;7&lt;WRU97&gt;F!!V!#A!(1X6S=G6O&gt;!!_1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"P&gt;81!!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!65'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!"Q!*!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="Set.vi" Type="VI" URL="../Set.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!='F"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#ZM&gt;G.M98.T!!!75'^X:8*4&gt;8"Q&lt;(F$;'&amp;O&lt;G6M)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!31#%-1WBB&lt;GZF&lt;&amp;.U982F!!!]1(!!(A!!("J1&lt;X&gt;F=F.V=("M?5.I97ZO:7QO&lt;(:D&lt;'&amp;T=Q!!&amp;6"P&gt;W6S5X6Q='RZ1WBB&lt;GZF&lt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
</LVClass>
