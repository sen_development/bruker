RSRC
 LVCCLBVW  (  T         � � 0           < � @�      ����            Ťf��ZN����U�@j          x�,dM�EA��ڗ�~���ُ ��	���B~       �.F4EOM�7���M��   ������ُ ��	���B~   �ʞ:�6��]y�nbO           LVCC              &   &x�c�d`j`�� Č@������P �T
�     T  'x�c`��� H1200� �,h�`Ʀ&�e..����P7������ ��c�#D#P	� �	$���c� �'ЍeA  �*5    VIDS       �  <x�{����ilaf������� ��������� �3@�
�A���B7<p8���[���7<�.*�5*L�"�>*"�>*,�,*/��������n���6 ��@��.* �f��3@�`� M�h�Pf(a1<u�x�	#�b��Q���5� jQ�.Q��l����> ��M<P�6��i=�?�@	+���������@ kn>�B�)��N� 9���`��8��� :H��C��F �w"�
�t�pHw���Ot���j������@�	�,�0��َ;h��� �2 T�* Q;�.8�wk_��
w&�85 b�z=FF�#C-T��f������ �!$�#H�\Py$3[���dl�b ��rT�(6ʞd'@ٳ�lF{��������lgW�4� U�&      �  16.0     �   16.0     �  16.0     �   16.0     �  16.0                       8   4Specifies the FPGA IO items for the I2C port to use.   ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP       � �                displayFilter �                    tdData �                IOInterface �      0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �      0����      FPGA I/O       � �                displayFilter �                    tdData �                IOInterface �      0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �      0����      FPGA I/O       f  \x��V�Oe�>��xZ��!��0��1]�X�M���p2ւL^l.\�.k���f���f��/L��o%�o�)���,ј��4�_�-!,���}��kK��,�X��������|>wP{Vj
p�"���	~�  ���i���1HL8GG�{B���Ќ(=���C7���#���]R��M�׌y@�K��mʬ׍�a�t���Jh�^��X�Nv�c� D�E=<tფ�����>$ը�(���K��S
]������);aqq��Uc����#������i@̔���1~��:��:j��5띁*�ϫ��P�M�#/��ԐylK\ʄF%�C���v�ԝ� $w�Z��`Q�<���t)��	/��>��V� ����ѥa?�����Sg%(���T1���N}��l�"���lN�֖A�p&������y�%���(�"N�ԗ���n�ҎX�Ҏ^�Eރ��O�/�mE�W���;�z���C�TY�lq�R_�y����Z�THA>����mP�R��T�a�\۠�8�w�TS��J����J����T�El����z��X��:S����0)�Z��F�ݐ�?^�߬�:p<.���I�yV�] �V-��}���oL�Jh���qUP�!�pTV���՚GUR�mAsʼ��f���!{g�_�
��s􅜊ߚ�Ќv��-���T	w��̷kY&ǫ=�����v�S��N΀	��3�|��M���`��pVQ��Ӟ�l�@���z��N�Y���2��Z�E3�Mh{b������\�V��ۻ�g��%�mo��r�?v�#c)Lح���^��f���<������]��~*�v��Rv^�gf�Ot�97!ވ⍌
˿�>q��@izh&9��fƓ��̅�����e�P&}咖�L-�����HN^J����jBi���ٵD|�9h���.����˛[>ؙ]^�Wv�������r�����~��1���_�;��◾g�K����m�r�?��w�_<;嗁-�����7X�z��V�ůW�PTke��׃��'2�W�H}X�	MjoÒt�}tѓ�O���]�Lo����/�ܩ"R��o���]���D�%�        <           BDHP        e   ux�c``(�`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-���W)b��z�\�8Se�<� e�                    h   (                                        �                    �     @UUA N 	                                                   ��*>  �>��*>  �>     @   ?          s  qx��U�r�@>%D(�-VK���5cju�M���8ka�����	Y\6�pՇ��|}=ل�P�L�wfg2�����o' x�~����W�������5A�k�4-)É�2���FEKŘ9*�
!$qI`M���>��c�RM!�7]���E8R�u���ӑ7��f�sl	��ٖ�-�s�P#F�;�,�^0")q'���7�����Όf(X����~��Kry� �Ni�Ɣ{��٤��'jS�I}�p�'�Aـ����rZ*]*��Ie�OEȃk����C�|]�fJ�^�k�n8c܆tC1i[�ʎ�gq,@��Q��]�D�%5m��V/е��W���S�1�������0��*m%}c���#������?��Gv�$��n=l��Ҋ>F]����"-uz�������˱�u�=��m��A�����0�v�	p?�/�^?��?��-D��F��[�Pk����B)���*� ����$.<�{��(���#(7��q�2�Ȥ��c*J�p���d'������y;p6tX�6�/D��c43tB�
Ў�V,W1Y�#Y�\�[�-[��mVq8J��p5�����d��o��[    e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  (  T                     4  <   LVSR      `RTSG      tCCST      �LIvi      �CONP      �TM80      �DFDS      �LIds      �VICD       vers     GCPR      xSTRG      �ICON      �icl8      �CPC2      �LIfp      �STR      �FPHb      FPSE      ,VPDP      @LIbd      TBDHb      hBDSE      |DTHP      �MUID      �HIST      �PRT       �VCTP      �FTAB      �                        ����       �        ����       �        ����       �        ����       �        ����       �        ����              ����      \        ����      l       ����      $       ����      4       ����      D       	����      T       
����      d        ����      t        ����      �        ����      �        ����      H        ����      L        ����      T       ����      d       ����      d        ����      d        ����      �        ����      �        ����      �        ����      �        ����      \        ����      d        ����      l        ����      t        ����      �        ����      $       �����      �    FPGA_I2C IO Cluster.ctl