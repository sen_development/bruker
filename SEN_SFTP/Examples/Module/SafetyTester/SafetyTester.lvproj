﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="Mein Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Devices" Type="Folder" URL="../Devices">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="CompleteMeasurement.vi" Type="VI" URL="../Implementations/CompleteMeasurement.vi"/>
		<Item Name="SafetyTester.lvclass" Type="LVClass" URL="../SafetyTester.lvclass"/>
		<Item Name="Abhängigkeiten" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Device_close.vi" Type="VI" URL="/&lt;instrlib&gt;/IF-XX/Common/Device_close.vi"/>
				<Item Name="Device_scan.vi" Type="VI" URL="/&lt;instrlib&gt;/IF-XX/Common/Device_scan.vi"/>
				<Item Name="Device_select.vi" Type="VI" URL="/&lt;instrlib&gt;/IF-XX/Common/Device_select.vi"/>
				<Item Name="ELR9000.vi" Type="VI" URL="/&lt;instrlib&gt;/IF-XX/ELR9 Series/ELR9000.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
			</Item>
			<Item Name="ELR9000Series.lvlib" Type="Library" URL="../../ElectronicLoad/Devices/ELR9000Series.lvlib"/>
			<Item Name="Load.lvlib" Type="Library" URL="../../ElectronicLoad/Lib/Load.lvlib"/>
			<Item Name="SCPI.lvlib" Type="Library" URL="../../CommunicationProtocol/Ethernet/Protocols/SCPI/SCPI.lvlib"/>
			<Item Name="SenDevice.lvclass" Type="LVClass" URL="../../DeviceClassSEN/SenDevice.lvclass"/>
		</Item>
		<Item Name="Build-Spezifikationen" Type="Build"/>
	</Item>
</Project>
